# linux-yocto-custom_x.x.x.bb 
# attempt to have only kernel version related stuff in here

require recipes-kernel/linux/linux-yocto-custom.inc

KBRANCH = "linux-6.6.y"

LINUX_VERSION = "6.6.15"

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"


# author	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2024-01-31 16:19:14 -0800
# committer	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2024-01-31 16:19:14 -0800
# commit	51f354b815c44f1e423edb3f089ceece9bd26976 (patch)
# tree		1b68f6e28e426a7b64170f41c4b165d9f58be024
# parent	ee82479f5d740968828a1fde41598f1d1e62eee6 (diff)
# download	linux-6.6.y.tar.gz

SRCREV ?= "51f354b815c44f1e423edb3f089ceece9bd26976"
#SRCREV_the-kernel:pn-linux-yocto-custom := "d2a6dc4eaf6d50ba32a9b39b4c6ec713a92072ab"
# in order to make buildhistory-collect-srcrevs happy:
SRCREV_the-kernel:pn-linux-yocto-custom := "${SRCREV}"

PATCHPATH="${THISDIR}/patch/6.6.x"

FILESEXTRAPATHS:prepend := "${PATCHPATH}:"

SRC_URI += "\
           file://multi-v7-ml-user-patches.scc \
           "
SRC_URI:append = " \
                file://${PATCHPATH}/patches;type=kmeta;destsuffix=patches \
                "

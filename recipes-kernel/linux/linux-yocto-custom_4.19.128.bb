# linux-yocto-custom_x.y.z.bb 
# attempt to have only kernel version related stuff in here

require recipes-kernel/linux/linux-yocto-custom.inc

KBRANCH = "linux-4.19.y"

LINUX_VERSION = "4.19.128"

LIC_FILES_CHKSUM = "file://COPYING;md5=bbea815ee2795b2f4230826c0c6b8814"

# author	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2020-06-10 21:35:02 +0200
# committer	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2020-06-10 21:35:02 +0200
# commit	3fc898571b974f9a05e4e5c1fe17b18548207091 (patch)
# tree		788f662db922525e5dada6aac5d2805eff50774a
# parent	59ecec707d7e0bc58095f2413bdaf004efc605e9 (diff)
# download	linux-3fc898571b974f9a05e4e5c1fe17b18548207091.tar.gz
# Linux 4.19.128 v4.19.128

SRCREV ?= "3fc898571b974f9a05e4e5c1fe17b18548207091"

PATCHPATH="${THISDIR}/patch/4.19.x"

FILESEXTRAPATHS:prepend := "${PATCHPATH}:"

SRC_URI += "\
           file://multi-v7-ml-user-patches.scc \
           "
SRC_URI:append = " \
                file://${PATCHPATH}/patches;type=kmeta;destsuffix=patches \
                "

# linux-yocto-custom_x.x.x.bb 
# attempt to have only kernel version related stuff in here

require recipes-kernel/linux/linux-yocto-custom.inc

KBRANCH = "linux-4.19.y"

LINUX_VERSION = "4.19.92"

LIC_FILES_CHKSUM = "file://COPYING;md5=bbea815ee2795b2f4230826c0c6b8814"

# author        Greg Kroah-Hartman <gregkh@linuxfoundation.org> 2019-12-31 16:36:37 +0100
# committer     Greg Kroah-Hartman <gregkh@linuxfoundation.org> 2019-12-31 16:36:37 +0100
# commit        c7ecf3e3a71c216327980f26b1e895ce9b07ad31 (patch)
# tree          16116f4a2c74c64b3bb8bab3f049c298e407f21a
# parent        ecfb25a7b1cdbeaf8017c3e5d724f62c74f06569 (diff)
# download      linux-c7ecf3e3a71c216327980f26b1e895ce9b07ad31.tar.gz
# Linux 4.19.92 v4.19.92 linux-4.19.y

SRCREV ?= "c7ecf3e3a71c216327980f26b1e895ce9b07ad31"

PATCHPATH="${THISDIR}/patch/4.19.x"

FILESEXTRAPATHS:prepend := "${PATCHPATH}:"

SRC_URI += "\
           file://multi-v7-ml-user-patches.scc \
           "
SRC_URI:append = " \
                file://${PATCHPATH}/patches;type=kmeta;destsuffix=patches \
                "

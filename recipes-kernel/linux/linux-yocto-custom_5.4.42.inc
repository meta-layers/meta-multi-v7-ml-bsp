# linux-yocto-custom_x.x.x.bb 
# attempt to have only kernel version related stuff in here

require recipes-kernel/linux/linux-yocto-custom.inc

KBRANCH = "linux-5.4.y"

LINUX_VERSION = "5.4.42"

LIC_FILES_CHKSUM = "file://COPYING;md5=bbea815ee2795b2f4230826c0c6b8814"

# author	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2020-05-20 08:20:41 +0200
# committer	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2020-05-20 08:20:41 +0200
# commit	1cdaf895c99d319c0007d0b62818cf85fc4b087f (patch)
# tree		96896c8b31ce579dac8444acfa5ae0b9dd5146bd
# parent	ecb3f529a554fcd5b58afde1fe69be771f41901a (diff)
# download	linux-1cdaf895c99d319c0007d0b62818cf85fc4b087f.tar.gz
# Linux 5.4.42 v5.4.42 linux-5.4.y

SRCREV ?= "1cdaf895c99d319c0007d0b62818cf85fc4b087f"

PATCHPATH="${THISDIR}/patch/5.4.x"

FILESEXTRAPATHS:prepend := "${PATCHPATH}:"

SRC_URI += "\
           file://multi-v7-ml-user-patches.scc \
           "
SRC_URI:append = " \
                file://${PATCHPATH}/patches;type=kmeta;destsuffix=patches \
                "

# linux-yocto-custom_x.x.x.bb 
# attempt to have only kernel version related stuff in here

require recipes-kernel/linux/linux-yocto-custom.inc

KBRANCH = "linux-5.10.y"

LINUX_VERSION = "5.10.2"

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

# author	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2020-12-21 13:30:08 +0100
# committer	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2020-12-21 13:30:08 +0100
# commit	d1988041d19dc8b532579bdbb7c4a978391c0011 (patch)
# tree		f4b3f81bf9527ef8b1e7edd04a4605a8bf05a9f0
# parent	dadaf794f207ebd94a42bf786f14bdcea95f8ec6 (diff)
# download	linux-d1988041d19dc8b532579bdbb7c4a978391c0011.tar.gz
# Linux 5.10.2 v5.10.2 linux-5.10.y

SRCREV ?= "d1988041d19dc8b532579bdbb7c4a978391c0011"

PATCHPATH="${THISDIR}/patch/5.10.x"

FILESEXTRAPATHS:prepend := "${PATCHPATH}:"

SRC_URI += "\
           file://multi-v7-ml-user-patches.scc \
           "
SRC_URI:append = " \
                file://${PATCHPATH}/patches;type=kmeta;destsuffix=patches \
                "

# linux-yocto-custom_x.x.x.bb 
# attempt to have only kernel version related stuff in here

require recipes-kernel/linux/linux-yocto-custom.inc

KBRANCH = "linux-6.1.y"

LINUX_VERSION = "6.1.71"

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

# author	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2024-01-05 15:18:41 +0100
# committer	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2024-01-05 15:18:41 +0100
# commit	38fb82ecd144fa22c5e41cb6e56f1fa8c98d6f61 (patch)
# tree		d189cddb6ddb64a6e53cc472da52cb14f38d3c5b
# parent	74c4c7d57cf2fbb6f596c6b16f5dcf6e4f9a0da3 (diff)
# download	linux-6.1.y.tar.gz

SRCREV ?= "38fb82ecd144fa22c5e41cb6e56f1fa8c98d6f61"
#SRCREV_the-kernel:pn-linux-yocto-custom := "d2a6dc4eaf6d50ba32a9b39b4c6ec713a92072ab"
# in order to make buildhistory-collect-srcrevs happy:
SRCREV_the-kernel:pn-linux-yocto-custom := "${SRCREV}"

PATCHPATH="${THISDIR}/patch/6.1.x"

FILESEXTRAPATHS:prepend := "${PATCHPATH}:"

SRC_URI += "\
           file://multi-v7-ml-user-patches.scc \
           "
SRC_URI:append = " \
                file://${PATCHPATH}/patches;type=kmeta;destsuffix=patches \
                "

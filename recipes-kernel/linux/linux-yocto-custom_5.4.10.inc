# linux-yocto-custom_x.x.x.bb 
# attempt to have only kernel version related stuff in here

require recipes-kernel/linux/linux-yocto-custom.inc

KBRANCH = "linux-5.4.y"

LINUX_VERSION = "5.4.10"

LIC_FILES_CHKSUM = "file://COPYING;md5=bbea815ee2795b2f4230826c0c6b8814"

# author	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2020-01-09 10:25:53 +0100
# committer	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2020-01-09 10:25:53 +0100
# commit	7a02c193298ec15f2ba1344b6bcd5d578a41b2e0 (patch)
# tree		1d6790a0156275c7d80fe5641d1df9ae6996f7d8
# parent	d7742abfe65263b2e683380bca1686657677cf04 (diff)
# download	linux-7a02c193298ec15f2ba1344b6bcd5d578a41b2e0.tar.gz
# Linux 5.4.10 v5.4.10 linux-5.4.y

SRCREV ?= "7a02c193298ec15f2ba1344b6bcd5d578a41b2e0"

PATCHPATH="${THISDIR}/patch/5.4.x"

FILESEXTRAPATHS:prepend := "${PATCHPATH}:"

SRC_URI += "\
           file://multi-v7-ml-user-patches.scc \
           "
SRC_URI:append = " \
                file://${PATCHPATH}/patches;type=kmeta;destsuffix=patches \
                "

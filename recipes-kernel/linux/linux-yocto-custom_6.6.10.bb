# linux-yocto-custom_x.x.x.bb 
# attempt to have only kernel version related stuff in here

require recipes-kernel/linux/linux-yocto-custom.inc

KBRANCH = "linux-6.6.y"

LINUX_VERSION = "6.6.10"

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

# author	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2024-01-05 15:19:45 +0100
# committer	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2024-01-05 15:19:45 +0100
# commit	c9a51ebb4bac69ed3fee9c0ebe0c2b5149e80845 (patch)
# tree		3b3a133539c4d035002956d07ae02c1a3418d525
# parent	9b603077e29c84836a44325593e959da818274c7 (diff)
# download	linux-c9a51ebb4bac69ed3fee9c0ebe0c2b5149e80845.tar.gz

SRCREV ?= "c9a51ebb4bac69ed3fee9c0ebe0c2b5149e80845"
#SRCREV_the-kernel:pn-linux-yocto-custom := "d2a6dc4eaf6d50ba32a9b39b4c6ec713a92072ab"
# in order to make buildhistory-collect-srcrevs happy:
SRCREV_the-kernel:pn-linux-yocto-custom := "${SRCREV}"

PATCHPATH="${THISDIR}/patch/6.6.x"

FILESEXTRAPATHS:prepend := "${PATCHPATH}:"

SRC_URI += "\
           file://multi-v7-ml-user-patches.scc \
           "
SRC_URI:append = " \
                file://${PATCHPATH}/patches;type=kmeta;destsuffix=patches \
                "

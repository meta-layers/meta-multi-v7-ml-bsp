# linux-yocto-custom_x.x.x.bb 
# attempt to have only kernel version related stuff in here

require recipes-kernel/linux/linux-yocto-custom.inc

#KBRANCH = "linux-5.15.y"
KBRANCH = "v5.15.35-phy"

LINUX_VERSION = "5.15.35"

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"


#@ author	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2022-08-03 12:03:56 +0200
#@ committer	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2022-08-03 12:03:56 +0200
#@ commit	d676d6149a2f4b4d66b8ea0a1dfef30a54cf5750 (patch)
#@ tree		e53285c30765ada2461ef1c7112466a0d3716cf8
#@ parent	f0e42e43795db4c1054295c0d35e79203db83449 (diff)
#@ download	linux-d676d6149a2f4b4d66b8ea0a1dfef30a54cf5750.tar.gz

#@ Linux 5.15.59 v5.15.59 linux-5.15.y

# v5.15.35-phy, tag: v5.15.35-phy1, origin/v5.15.35-phy

#SRCREV ?= "d676d6149a2f4b4d66b8ea0a1dfef30a54cf5750"
SRCREV ?= "bd7dc9b4693c0068f79180da7d2c36fbad865671"

PATCHPATH="${THISDIR}/patch/5.15.x"

FILESEXTRAPATHS:prepend := "${PATCHPATH}:"

SRC_URI += "\
           file://multi-v7-ml-user-patches.scc \
           "
SRC_URI:append = " \
                file://${PATCHPATH}/patches;type=kmeta;destsuffix=patches \
                "

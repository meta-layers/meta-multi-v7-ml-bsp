# linux-yocto-custom_x.x.x.bb 
# attempt to have only kernel version related stuff in here

require recipes-kernel/linux/linux-yocto-custom.inc

KBRANCH = "linux-5.10.y"

LINUX_VERSION = "5.10.1"

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

# author	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2020-12-14 19:33:01 +0100
# committer	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2020-12-14 19:33:01 +0100
# commit	841fca5a32cccd7d0123c0271f4350161ada5507 (patch)
# tree		381ca09f351d1e51fab362b5c390e16670572e04
# parent	26934c83005e75eab2b8d54d0fa5adbee4f27535 (diff)
# download	linux-841fca5a32cccd7d0123c0271f4350161ada5507.tar.gz
# Linux 5.10.1 v5.10.1 linux-5.10.y

SRCREV ?= "841fca5a32cccd7d0123c0271f4350161ada5507"

PATCHPATH="${THISDIR}/patch/5.10.x"

FILESEXTRAPATHS:prepend := "${PATCHPATH}:"

SRC_URI += "\
           file://multi-v7-ml-user-patches.scc \
           "
SRC_URI:append = " \
                file://${PATCHPATH}/patches;type=kmeta;destsuffix=patches \
                "

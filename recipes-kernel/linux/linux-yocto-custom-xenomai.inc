# xenomai common stuff 

# the ipipe patch is part of our meta data e.g.
# meta-multi-v7-ml-bsp/recipes-kernel/linux/xenopatch/x.y.z
SRC_URI += "file://${IPIPE_PATCH};apply=0"

# --> we want the /scripts/prepare-kernel.sh script, which comes from the xenomai (user space) repo
# for some reason when I include the whole xenomai repo it tries to get the kernel branch,
# so as a workaround I use subpath(s), which seems to work
SRC_URI += "git://gitlab.denx.de/Xenomai/xenomai.git;protocol=https;branch=master;subpath=scripts;name=prepare-kernel"
SRC_URI += "git://gitlab.denx.de/Xenomai/xenomai.git;protocol=https;branch=master;subpath=config;name=prepare-kernel"
SRC_URI += "git://gitlab.denx.de/Xenomai/xenomai.git;protocol=https;branch=master;subpath=include;name=prepare-kernel"
SRC_URI += "git://gitlab.denx.de/Xenomai/xenomai.git;protocol=https;branch=master;subpath=kernel;name=prepare-kernel"

SRCREV_FORMAT = "the-kernel_prepare-kernel"

do_configure:prepend () {
    # Prepare kernel
    ${WORKDIR}/scripts/prepare-kernel.sh --arch=${ARCH} --linux=${S} --ipipe="${WORKDIR}/${IPIPE_PATCH}" --default
}
# <-- we want the /scripts/prepare-kernel.sh script, which comes from the xenomai (user space) repo

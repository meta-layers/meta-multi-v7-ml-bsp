I downloaded .patch.xz from here[1] and made like this from .patch.xz -> .patch

xzcat fake-patch-5.10.27-rt35.patch.xz > fake-patch-5.10.27-rt35.patch

[1] https://mirrors.edge.kernel.org/pub/linux/kernel/projects/rt/

Note that I call it fake patch because it's not official.
I just renamed 5.10.25 to 5.10.27 and it applied happily.

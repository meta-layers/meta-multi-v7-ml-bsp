# linux-yocto-custom_x.x.x.bb 
# attempt to have only kernel version related stuff in here

require recipes-kernel/linux/linux-yocto-custom.inc

KBRANCH = "linux-5.10.y"

LINUX_VERSION = "5.10.27"

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

# author	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2021-03-30 14:32:09 +0200
# committer	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2021-03-30 14:32:09 +0200
# commit	472493c8a425f62200882c2c6acb1be2e29b3c03 (patch)
# tree		a18c299afdf8bfb0522909d925bcba3ce74593f7
# parent	3a1ca9bd4f5a647439e82e07b03d072781d9d180 (diff)
# download	linux-472493c8a425f62200882c2c6acb1be2e29b3c03.tar.gz
# Linux 5.10.27 v5.10.27 linux-5.10.y

SRCREV ?= "472493c8a425f62200882c2c6acb1be2e29b3c03"

PATCHPATH="${THISDIR}/patch/5.10.x"

FILESEXTRAPATHS:prepend := "${PATCHPATH}:"

SRC_URI += "\
           file://multi-v7-ml-user-patches.scc \
           "
SRC_URI:append = " \
                file://${PATCHPATH}/patches;type=kmeta;destsuffix=patches \
                "

# linux-yocto-custom_x.x.x.bb 
# attempt to have only kernel version related stuff in here

require recipes-kernel/linux/linux-yocto-custom.inc

KBRANCH = "master"

LINUX_VERSION = "6.12.0-rc6"

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

# author	Linus Torvalds <torvalds@linux-foundation.org>	2024-11-03 14:05:52 -1000
# committer	Linus Torvalds <torvalds@linux-foundation.org>	2024-11-03 14:05:52 -1000
# commit	59b723cd2adbac2a34fc8e12c74ae26ae45bf230 (patch)
# tree	        2435c94eb7bcf41b1a5b8a1b99896c10d22518d5
# parent	a8cc7432728d019a10cb412401ebc15ed7504289 (diff)
# download	linux-59b723cd2adbac2a34fc8e12c74ae26ae45bf230.tar.gz
# Linux 6.12-rc6 v6.12-rc6

SRCREV ?= "59b723cd2adbac2a34fc8e12c74ae26ae45bf230"
#SRCREV_the-kernel:pn-linux-yocto-custom := "d2a6dc4eaf6d50ba32a9b39b4c6ec713a92072ab"
# in order to make buildhistory-collect-srcrevs happy:
SRCREV_the-kernel:pn-linux-yocto-custom := "${SRCREV}"

PATCHPATH="${THISDIR}/patch/6.12.x"

FILESEXTRAPATHS:prepend := "${PATCHPATH}:"

SRC_URI += "\
           file://multi-v7-ml-user-patches.scc \
           "
SRC_URI:append = " \
                file://${PATCHPATH}/patches;type=kmeta;destsuffix=patches \
                "

# linux-yocto-custom CVE-1999-0656     5.0      0.0      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-1999-0656
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-1999-0656
# does not exist
#
# https://ubuntu.com/security/CVE-1999-0656
# Cannot find a CVE with ID ‘CVE-1999-0656’
#
# https://security-tracker.debian.org/tracker/CVE-1999-0656
# The ugidd RPC interface, by design, allows remote attackers to enumerate valid usernames
# by specifying arbitrary UIDs that ugidd maps to local user and group names.
# NOT-FOR-US: Data pre-dating the Security Tracker
#
# Mitigation:
# Remove or disable with rpc.ugidd daemon, if it is not necessary in your configuration
CVE_STATUS[CVE-1999-0656] = "ignored: cpe:*:linux_kernel:There is no rpc.ugidd daemon running."


# linux-yocto-custom CVE-2006-2932     4.9      0.0      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2006-2932
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2006-2932
# does not exist
#
# https://ubuntu.com/security/CVE-2006-2932
# Does not apply to software found in Ubuntu.
#
# https://security-tracker.debian.org/tracker/CVE-2006-2932
# A regression error in the restore_all code path of the 4/4GB split support for non-hugemem
# Linux kernels on Red Hat Linux Desktop and Enterprise Linux 4 allows local users to cause a denial of service (panic) via unspecified vectors.
# - linux-2.6 <not-affected> (vulnerable code not present)
CVE_STATUS[CVE-2006-2932] = "not-applicable-platform: Issue only applies on Red Hat Linux Desktop and Enterprise Linux 4"


# linux-yocto-custom CVE-2007-2764     7.8      0.0      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2007-2764
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2007-2764
# does not exist
#
# https://ubuntu.com/security/CVE-2007-2764
# Does not apply to software found in Ubuntu.
#
# https://security-tracker.debian.org/tracker/CVE-2007-2764
# The embedded Linux kernel in certain Sun-Brocade SilkWorm switches before 20070516 does not properly handle a situation
# in which a non-root user creates a kernel process, which allows attackers to cause a denial of service 
# (oops and device reboot) via unspecified vectors.
# NOT-FOR-US: Sun-Brocade SilkWorm
CVE_STATUS[CVE-2007-2764] = "not-applicable-platform: Issue only applies on certain Sun-Brocade SilkWorm switches before 20070516"


# linux-yocto-custom CVE-2007-4998     6.9      0.0      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2007-4998
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2007-4998
# does not exist
#
# https://ubuntu.com/security/CVE-2007-4998
# cp, when running with an option to preserve symlinks on multiple OSes, allows local,
# user-assisted attackers to overwrite arbitrary files via a symlink attack using crafted
# directories containing multiple source files that are copied to the same destination.
#
# coreutils should not be affected. This problem is many years old. busybox 1.6.1 and 1.9.0 known not to be affected
#
# https://security-tracker.debian.org/tracker/CVE-2007-4998
# coreutils	source	(unstable)	4.1.2 - fixed version
#
# coreutils                                             :9.5-r0                                                    
# coreutils-native                                      :9.5-r0                                                    
# nativesdk-coreutils                                   :9.5-r0                                                    
# uutils-coreutils                                   :0.0.27-r0         
#
# busybox                                            :1.36.1-r0                                                    
# busybox-inittab                                    :1.36.1-r0      
#
# !!! I am not quite sure why this shows up in the kernel CVEs
CVE_STATUS[CVE-2007-4998] = "ignored: cpe:*:linux_kernel:It turns out that this is a not related to the kernel, but coreutils/busybox and coreutils:9.5-r0, busybox:1.36.1-r0 should be fixed"


# linux-yocto-custom CVE-2008-2544     2.1      5.5      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2008-2544
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2008-2544
# does not exist
#
# https://ubuntu.com/security/CVE-2008-2544
# according to second debian bug report, this was fixed sometime in the 2.6.29 timeframe, marking not-affected
#
# https://security-tracker.debian.org/tracker/CVE-2008-2544
# Mounting /proc filesystem via chroot command silently mounts it in read-write mode.
# The user could bypass the chroot environment and gain write access to files, he would never have otherwise.
# vulnerable/unfixed/unimportant
# 
# https://bugzilla.redhat.com/show_bug.cgi?id=213135 - at the very bottom:
# It turns out that this is a non-issue. vfsmounts are separate, while superblock is shared. If you want your vfsmount to be read-only, you need to:
#
# mount -o remount,ro --bind <your mount point>
#
# Filesystem itself will remain r/w (it *is* shared with the entire system), but any write access to it via that mount will fail with EROFS until you remount it rw on vfsmount level (as above, with s/ro/rw/).
# 
# To mount this read-only, you need to do this:
# mount -t proc none /tmp/proc
# mount -o remount,ro --bind /tmp/proc 
# mount | grep tmp.proc
# none on /tmp/proc type proc (ro,bind)
# grep tmp.proc /proc/mounts 
# none /tmp/proc proc ro 0 0
# echo 1 > /tmp/proc/sys/kernel/vsyscall64 
# -bash: vsyscall64: Read-only file system
# 
# This is documented in mount(2).
# 
# Thanks.
CVE_STATUS[CVE-2008-2544] = "ignored: cpe:*:linux_kernel:It turns out that this is a non-issue. vfsmounts are separate, while superblock is shared."


# linux-yocto-custom CVE-2014-8171     4.9      5.5      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2014-8171
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2014-8171
# does not exist
#
# https://ubuntu.com/security/CVE-2014-8171
# break-fix: 1da177e4c3f41524e886b7f1b8a0c1fc7321cac2 - 3812c8c8f3953921ef18544110dafc3505c1ac62
# break-fix: 1da177e4c3f41524e886b7f1b8a0c1fc7321cac2 - 4942642080ea82d99ab5b653abb9a12b7ba31f4a
#
# git describe --contains 3812c8c8f3953921ef18544110dafc3505c1ac62
# v3.12-rc1~24^2~18
# git describe --contains 4942642080ea82d99ab5b653abb9a12b7ba31f4a
# v3.12-rc6~11^2~8
CVE_STATUS[CVE-2014-8171] = "fixed-version: Fixed from version v3.12-rc6"


# linux-yocto-custom CVE-2016-0774     5.6      6.8      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2016-0774
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2016-0774
# does not exist
#
# https://ubuntu.com/security/CVE-2016-0774
#
# The (1) pipe_read and (2) pipe_write implementations in fs/pipe.c in a certain Linux kernel backport in the linux
# package before 3.2.73-2+deb7u3 on Debian wheezy and the kernel package before 3.10.0-229.26.2 on Red Hat Enterprise
# Linux (RHEL) 7.1 do not properly consider the side effects of failed __copy_to_user_inatomic and __copy_from_user_inatomic
# calls, which allows local users to cause a denial of service (system crash) or possibly gain privileges via a crafted application,
# aka an "I/O vector array overrun." NOTE: this vulnerability exists because of an incorrect fix for CVE-2015-1805.
# 
CVE_STATUS[CVE-2016-0774] = "not-applicable-platform: Issue only applies on linux package before 3.2.73-2+deb7u3 on Debian wheezy and the kernel package before 3.10.0-229.26.2 on Red Hat Enterprise Linux (RHEL) 7.1"

# linux-yocto-custom CVE-2016-3695     2.1      5.5      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2016-3695
# 
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2016-3695
# does not exist
#
# https://ubuntu.com/security/CVE-2016-3695
# break-fix: https://git.kernel.org/linus/local-lockdown - https://git.kernel.org/linus/local-2016-3695-1
# This CVE was assigned against an out-of-tree patch series.
#
# https://security-tracker.debian.org/tracker/CVE-2016-3695
# The einj_error_inject function in drivers/acpi/apei/einj.c in the Linux kernel allows local users to simulate
# hardware errors and consequently cause a denial of service by leveraging failure to disable APEI error injection through EINJ when securelevel is set.
# [jessie] - linux <not-affected> (Vulnerable code not present)
# [wheezy] - linux <not-affected> (Vulnerable code not present)
#
# https://github.com/mjg59/linux/commit/d7a6be58edc01b1c66ecd8fcc91236bfbce0a420
# drivers/acpi/apei/einj.c
# 
CVE_STATUS[CVE-2016-3695] = "not-applicable-config: CONFIG_ACPI is not set"


# linux-yocto-custom CVE-2016-3699     6.9      7.4      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2016-3699
# 
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2016-3699
# does not exist
#
# https://ubuntu.com/security/CVE-2016-3699
# no affected
#
# https://security-tracker.debian.org/tracker/CVE-2016-3699
# The Linux kernel, as used in Red Hat Enterprise Linux 7.2 and Red Hat Enterprise MRG 2 and when booted with UEFI Secure Boot enabled,
# allows local users to bypass intended Secure Boot restrictions and execute untrusted code by appending ACPI tables to the initrd.
# - linux <not-affected> (Fixed before we first included the securelevel patchset)
# https://github.com/mjg59/linux/commit/a4a5ed2835e8ea042868b7401dced3f517cafa76
# securelevel patchset added in 4.5.1-1
# arch/x86/kernel/setup.c
# drivers/acpi/osl.c
# 
# break-fix: - a4a5ed2835e8ea042868b7401dced3f517cafa76
# Yes but this does not exist anymore ;)
#
# https://kernel.dance/#a4a5ed2835e8ea042868b7401dced3f517cafa76
# {
# "commit": "a4a5ed2835e8ea042868b7401dced3f517cafa76",
# "details": [
#  "[!] GitHub error: No commit found for SHA: a4a5ed2835e8ea042868b7401dced3f517cafa76"
# ],
# "the commit landed on upstream on": [],
# "the commit is a backport of": [],
# "the commit was backported to": [],
# "the commit fixes a bug introduced by": [],
# "the buggy commit landed on upstream on": [],
# "the buggy commit was backported to": [],
# "the commit introduced a bug fixed by": [],
# "syzkaller reference for the commit and the fix commit": [],
# "cve identifier for the commit and the fix commit": [
#  {
#   "cve": "CVE-2016-3699"
#  }
# ]
#}
#
# I guess the easiest here would be to argue that we don't build for x86
#
# Symbol: ACPI [=ACPI]
# │ Type  : unknown
# │
# │
# │ Symbol: ACPI_ADXL [=ACPI_ADXL]
# │ Type  : unknown
# │ Selected by [n]:
# │   - EDAC_SKX [=n] && EDAC [=y] && PCI [=y] && X86_64 && X86_MCE_INTEL && PCI_MMCONFIG && ACPI && (ACPI_NFIT || !ACPI_NFIT)
# │   - EDAC_I10NM [=n] && EDAC [=y] && PCI [=y] && X86_64 && X86_MCE_INTEL && PCI_MMCONFIG && ACPI && (ACPI_NFIT || !ACPI_NFIT)
CVE_STATUS[CVE-2016-3699] = "not-applicable-config: CONFIG_ACPI is not set and we build for arm and not x86"



# linux-yocto-custom CVE-2017-1000255  6.6      5.5      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2017-1000255
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2017-1000255
# does not exist
#
# https://ubuntu.com/security/CVE-2017-1000255
# break-fix: 5d176f751ee3c6eededd984ad409bff201f436a7 - 265e60a170d0a0ecfc2d20490134ed2c48dd45ab
# break-fix: 5d176f751ee3c6eededd984ad409bff201f436a7 - 044215d145a7a8a60ffa8fdc859d110a795fa6ea
#
# git describe --contains 265e60a170d0a0ecfc2d20490134ed2c48dd45ab
# v4.14-rc5~39^2~1
# git describe --contains 044215d145a7a8a60ffa8fdc859d110a795fa6ea
# v4.14-rc5~39^2
#
# https://security-tracker.debian.org/tracker/CVE-2017-1000255
# On Linux running on PowerPC hardware (Power8 or later) a user process can craft a signal frame and
# then do a sigreturn so that the kernel will take an exception (interrupt), and use the r1 value *from the signal frame*
# as the kernel stack pointer. As part of the exception entry the content of the signal frame is written to the kernel stack,
# allowing an attacker to overwrite arbitrary locations with arbitrary values. The exception handling does produce an oops,
# and a panic if panic_on_oops=1, but only after kernel memory has been over written. This flaw was introduced in commit:
# "5d176f751ee3 (powerpc: tm: Enable transactional memory (TM) lazily for userspace)" which was merged upstream into v4.9-rc1.
# Please note that kernels built with CONFIG_PPC_TRANSACTIONAL_MEM=n are not vulnerable.
# 
# One could argue that we are not building for a PowerPC.
CVE_STATUS[CVE-2017-1000255] = "fixed-version: Fixed from version v4.14-rc5"


# linux-yocto-custom CVE-2017-1000377  4.6      5.9      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2017-1000377
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2017-1000377
# does not exist
#
# https://ubuntu.com/security/CVE-2017-1000377
# Does not apply to software found in Ubuntu.
#
# https://security-tracker.debian.org/tracker/CVE-2017-1000377
# An issue was discovered in the size of the default stack guard page on PAX Linux
# (originally from GRSecurity but shipped by other Linux vendors), 
# specifically the default stack guard page is not sufficiently large and can be "jumped" over 
# (the stack guard page is bypassed), this affects PAX Linux Kernel versions as of June 19, 2017 
# (specific version information is not available at this time).
# NOT-FOR-US: GRSecurity/PAX Linux specific assignment
CVE_STATUS[CVE-2017-1000377] = "not-applicable-platform: Issue only applies on GRSecurity/PAX Linux"


# linux-yocto-custom CVE-2017-6264     9.3      7.8      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2017-6264
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2017-6264
# does not exist
#
# https://ubuntu.com/security/CVE-2017-6264
# Does not apply to software found in Ubuntu.
# 
# https://security-tracker.debian.org/tracker/CVE-2017-6264
# An elevation of privilege vulnerability exists in the NVIDIA GPU driver (gm20b_clk_throt_set_cdev_state),
# where an out of bound memory read is used as a function pointer could lead to code execution in the kernel.
# This issue is rated as high because it could allow a local malicious application to execute arbitrary code within the context of a privileged process.
# Product: Android. Version: N/A. Android ID: A-34705430. References: N-CVE-2017-6264.
# NOT-FOR-US: NVIDIA components for Android
CVE_STATUS[CVE-2017-6264] = "not-applicable-platform: Issue only applies on NVIDIA components for Android"


# linux-yocto-custom CVE-2018-10840    7.2      5.2      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2018-10840
# 
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2018-10840
# does not exist
#
# https://ubuntu.com/security/CVE-2018-10840
# break-fix: dec214d00e0d78a08b947d7dccdfdb84407a9f4d - 8a2b307c21d4b290e3cbe33f768f194286d07c23
#
# git describe --contains 8a2b307c21d4b290e3cbe33f768f194286d07c23
# v4.18-rc1~143^2~1
CVE_STATUS[CVE-2018-10840] = "fixed-version: Fixed from version v4.18-rc1"


# linux-yocto-custom CVE-2018-10876    4.9      5.5      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2018-10876
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2018-10876
# break-fix: - 8844618d8aa7a9973e7b527d038a2a589665002c
# upstream_linux: released (4.18~rc4, 4.4.140)
#
# git describe --contains 8844618d8aa7a9973e7b527d038a2a589665002c
# v4.18-rc4~7^2~9
CVE_STATUS[CVE-2018-10876] = "fixed-version: Fixed from version v4.18-rc4"


# linux-yocto-custom CVE-2018-10882    4.9      5.5      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2018-10882
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2018-10882
# break-fix: - c37e9e013469521d9adb932d17a1795c139b36db
# upstream_linux: released (4.18~rc4, 4.4.140)
#
# git describe --contains c37e9e013469521d9adb932d17a1795c139b36db
# v4.18-rc4~7^2~2
CVE_STATUS[CVE-2018-10882] = "fixed-version: Fixed from version v4.18-rc4"


# linux-yocto-custom CVE-2018-10902    4.6      7.8      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2018-10902
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2018-10902
# break-fix: 1da177e4c3f41524e886b7f1b8a0c1fc7321cac2 39675f7a7c7e7702f7d5341f1e0d01db746543a0
# upstream_linux: released (4.18~rc6, 4.4.144)
#
# git describe --contains 39675f7a7c7e7702f7d5341f1e0d01db746543a0
# v4.18-rc6~26^2~1
CVE_STATUS[CVE-2018-10902] = "fixed-version: Fixed from version v4.18-rc6"



# linux-yocto-custom CVE-2018-14625    4.4      7.0      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2018-14625
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2018-14625
# does not exist
#
# https://ubuntu.com/security/CVE-2018-14625
# break-fix: 433fc58e6bf2c8bd97e57153ed28e64fd78207b8 - 834e772c8db0c6a275d75315d90aba4ebbb1e249
#
# git describe --contains 834e772c8db0c6a275d75315d90aba4ebbb1e249
# v4.20-rc6~17^2
CVE_STATUS[CVE-2018-14625] = "fixed-version: Fixed from version v4.20-rc6"



# linux-yocto-custom CVE-2018-6559     2.1      3.3      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2018-6559
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2018-6559
# does not exist
#
# https://ubuntu.com/security/CVE-2018-6559
# This CVE is specific to Ubuntu since Ubuntu allows overlayfs mounts inside of user namespaces.
# This flaw was previously discovered and fixed as part of CVE-2015-1328.
# The fix for CVE-2015-1328 was incorrectly dropped from the Ubuntu kernel during a merge with related changes in the upstream Linux kernel.
# CVE-2018-6559 represents the portion of CVE-2015-1328 that was incorrectly reintroduced into the Ubuntu kernel.
# 
# https://security-tracker.debian.org/tracker/CVE-2018-6559
# https://bugs.launchpad.net/ubuntu/+source/linux/+bug/1793458
#
CVE_STATUS[CVE-2018-6559] = "not-applicable-platform: Issue only applies on Ubuntu"


# linux-yocto-custom CVE-2019-14899    4.9      7.4      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2019-14899
# 
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2019-14899
# upstream_linux: deferred (2019-12-13)
#
# https://ubuntu.com/security/CVE-2019-14899
# No current fix from upstream as of 2019-12-13
# it is asserted that the Linux XFRM IPsec implementation does not allow bypassing by routing.
#
# https://security-tracker.debian.org/tracker/CVE-2019-14899
# A vulnerability was discovered in Linux, FreeBSD, OpenBSD, MacOS, iOS, and Android that allows a malicious access point, or an adjacent user,
# to determine if a connected user is using a VPN, make positive inferences about the websites they are visiting,
# and determine the correct sequence and acknowledgement numbers in use, allowing the bad actor to inject data into the TCP stream.
# This provides everything that is needed for an attacker to hijack active connections inside the VPN tunnel.
#
# https://www.openwall.com/lists/oss-security/2019/12/05/1
# 
# **Possible Mitigations:
#
# 1. Turning reverse path filtering on
#
# Potential problem: Asynchronous routing not reliable on mobile devices,
# etc. Also, it isn’t clear that this is actually a solution since it
# appears to work in other OSes with different networking stacks. Also,
# even with reverse path filtering on strict mode, the first two parts of
# the attack can be completed, allowing the AP to make inferences about
# active connections, and we believe it may be possible to carry out the
# entire attack, but haven’t accomplished this yet.
#
# 2. Bogon filtering
#
# Potential problem: Local network addresses used for vpns and local
# networks, and some nations, including Iran, use the reserved private IP
# space as part of the public space.
#
# 3. Encrypted packet size and timing
#
# Since the size and number of packets allows the attacker to bypass the
# encryption provided by the VPN service, perhaps some sort of padding
# could be added to the encrypted packets to make them the same size.
# Also, since the challenge ACK per process limit allows us to determine
# if the encrypted packets are challenge ACKs, allowing the host to
# respond with equivalent-sized packets after exhausting this limit could
# prevent the attacker from making this inference.
#
#
# We have prepared a paper for publication concerning this
# vulnerability and the related implications, but intend to keep it
# embargoed until we have found a satisfactory workaround. Then we will
# report the vulnerability to oss-security@...ts.openwall.com. We are
# also reporting this vulnerability to the other services affected, which
# also includes: Systemd, Google, Apple, OpenVPN, and WireGuard, in
# addition to distros@...openwall.org for the operating systems affected.
#
# https://wiki.yoctoproject.org/wiki/CVE_Status#CVE-2019-14899_(linux-yocto)
# Claims to be about breaking into VPN tunnels. OpenVPN dispute, Red Hat think it might actually have a larger scope but also the paper is misleading.
# 
# https://support.sophos.com/support/s/article/KBA-000007138?language=en_US
# Sophos has confirmed that the XG and UTM firewall devices are not affected by this as they utilize policy-based VPN technology and the threat only affects route-based VPNs.
# The Sophos SSL VPN client is not affected as it is based on OpenVPN. As per OpenVPN, the software is not affected. 
#
# https://seclists.org/oss-sec/2019/q4/123
# Hello List,
#
# Some important comments on the matter and especially in regards to IPsec:
# * This attack works regardless of if you have a VPN or not. The attacker just needs to be able to
#   send packets to the other host. It's not systemd specific. It can also occur because the user deliberately
#   configured the rp_filter that way (that's sometimes the case if PBR (Policy Based Routing) is configured.
#   The default for rp_filter is strict. For further information on the matter see ip-sysctl.txt[2]
#   and RFC 3704 Section 2.4[3]. For now, just create a file /etc/sysctl.d/51-rpfilter.conf with the content 
#   "net.ipv4.conf.all.rp_filter=1".
# * You can solve the problem generally for IPv6 by using the rpfilter iptables or nftables module in *mangle 
#   PREROUTING[1].
#   Just globally one rule is needed.
# * Only route based VPNs are impacted. In comparison, policy based VPNs are not impacted (On Linux only implementable 
#   using XFRM, which
#   is IPsec on Linux specific) unless the XFRM policy's level is set to "use" instead of "required" (default))
#   because any traffic received that matches a policy (IPsec security policy) and that is not protected is dropped.
#   An attacker could only inject packets by attacking the connection whenever it is unprotected (e.g. On a commercial 
#   VPN provider
#   setup that would be when the connection "comes" out of the VPN server and goes to the destination on the WAN).
#   So you're ususally fine. And even when a route based VPN is used, strict rp_filter can still save your bacon.
# 
# The probing of "virtual" IPv4 addresses can be made more difficult by configuring the VPN software to bind them to,
# for example, the loopback interface and setting arp_ignore of any interface facing a possible attacker to 2[2].
# That would prevent the sending of arp responses to arp requests for virtual IPs. I am not aware of a
# similiar setting for IPv6. That might be related to the lack of an rp_filter setting for IPv6 on Linux.
# Probing for addresses by using any protocol other than TCP (because TCP on Linux is handled in a special way
# in regards to routing. It sends the responses over the same interface and MAC addresses as the request was received,
# AFAIR) would not be possible if the response was to go over the VPN tunnel because the VPN server would most likely
# drop it as a martian (the destination would probably be a private network and they're not routable over the Internet.
# It has to be investigated on a case by case basis).
# 
# strongSwan by default binds any "virtual" IPs to the interface the route to the other peer goes over. You can change 
# that though.
# I don't know about libreswan or openswan (shouldn't use the last one anyway).
# 
# This vulnerability works against OpenVPN, WireGuard, and IKEv2/IPSec,
# but has not been thoroughly tested against tor, but we believe it is
# not vulnerable since it operates in a SOCKS layer and includes
# authentication and encryption that happens in userspace. 
# 
# It doesn't work against TOR because the destination address would be 127.0.0.1 and
# Linux (don't know about other operating systems) drops packets to that destination unless the input
# interface is loopback or route_localnet in sysctl of the input interface is set to 1 (used if services
# bound to localhost are exposed to the network via DNAT rules).
# 
# 3. Encrypted packet size and timing
# 
# Since the size and number of packets allows the attacker to bypass the
# encryption provided by the VPN service, perhaps some sort of padding
# could be added to the encrypted packets to make them the same size.
# Also, since the challenge ACK per process limit allows us to determine
# if the encrypted packets are challenge ACKs, allowing the host to
# respond with equivalent-sized packets after exhausting this limit could
# prevent the attacker from making this inference.
#
# IPsec supports that. It's called TFC (Traffic Flow Confidentiality). It can be configured to arbitrary values or to pad 
# up to the MTU of the link.
# It's disabled by default.
#
# Kind regards
# 
# Noel
# 
# [1] Would look like that: ip6tables -t mangle -I PREROUTING -m rpfilter --invert -j DROP
# [2] https://www.kernel.org/doc/Documentation/networking/ip-sysctl.txt
# [3] https://tools.ietf.org/html/rfc3704#section-2.4
# 
# https://security.stackexchange.com/questions/237391/wireguard-cve-2019-14899-how-secure-the-protocol-really-is
# 
# https://forum.netgate.com/topic/148713/cve-2019-14899/9
#
# https://marc.info/?l=oss-security&s=CVE-2019-14899
#
# 
# @@@TODO: not sure how to deal with this one


# linux-yocto-custom CVE-2019-3016     1.9      4.7      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2019-3016
# 
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2019-3016
# does not exist
# 
# https://ubuntu.com/security/CVE-2019-3016
# break-fix: f38a7b75267f1fb240a8178cbcb16d66dd37aac8 - 8c6de56a42e0c657955e12b882a81ef07d1d073e
#
# git describe --contains 8c6de56a42e0c657955e12b882a81ef07d1d073e
# v5.6-rc1~106^2^2~4
CVE_STATUS[CVE-2019-3016] = "fixed-version: Fixed from version v5.6-rc1"

# linux-yocto-custom CVE-2019-3819     4.9      4.2      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2019-3819
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2019-3819
# break-fix: 717adfdaf14704fd3ec7fa2c04520c0723247eac 13054abbaa4f1fd4e6f3b4b63439ec033b4c8035
# upstream_linux: released (5.0~rc6, 4.4.175)
# 
# git describe --contains 13054abbaa4f1fd4e6f3b4b63439ec033b4c8035
# v5.0-rc6~31^2
CVE_STATUS[CVE-2019-3819] = "fixed-version: Fixed from version v5.0-rc6"


# linux-yocto-custom CVE-2019-3887     4.7      6.7      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2019-3887
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2019-3887
# does not exist
# 
# https://ubuntu.com/security/CVE-2019-3887
# break-fix: 15303ba5d1cd9b28d03a980456c0978c0ea3b208 - acff78477b9b4f26ecdf65733a4ed77fe837e9dc
# break-fix: c992384bde84fb2f0318bdb4f6c710605dd7a217 - c73f4c998e1fd4249b9edfa39e23f4fda2b9b041
#
# git describe --contains acff78477b9b4f26ecdf65733a4ed77fe837e9dc
# v5.1-rc4~13^2~1
# git describe --contains c73f4c998e1fd4249b9edfa39e23f4fda2b9b041
# v5.1-rc4~13^2
CVE_STATUS[CVE-2019-3887] = "fixed-version: Fixed from version v5.1-rc4"


# linux-yocto-custom CVE-2020-10742    3.6      6.0      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2020-10742
# 
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2020-10742
# does not exist
#
# https://ubuntu.com/security/CVE-2020-10742
# break-fix: 1da177e4c3f41524e886b7f1b8a0c1fc7321cac2 - 91f79c43d1b54d7154b118860d81b39bad07dfff
#
# https://security-tracker.debian.org/tracker/CVE-2020-10742
#
# git describe --contains 91f79c43d1b54d7154b118860d81b39bad07dfff
# v3.16-rc1~36^2~49
CVE_STATUS[CVE-2020-10742] = "fixed-version: Fixed from version v3.16-rc1"

# linux-yocto-custom CVE-2020-16119    4.6      7.8      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2020-16119
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2020-16119
# does not exist
#
# https://ubuntu.com/security/CVE-2020-16119
# break-fix: 2677d20677314101293e6da0094ede7b5526d2b1 - d9ea761fdd197351890418acd462c51f241014a7
#
# https://security-tracker.debian.org/tracker/CVE-2020-16119
# Use-after-free vulnerability in the Linux kernel exploitable by a local attacker due to reuse of a DCCP socket with an attached dccps_hc_tx_ccid object as a listener after being released.
# Fixed in Ubuntu Linux kernel 5.4.0-51.56, 5.3.0-68.63, 4.15.0-121.123, 4.4.0-193.224, 3.13.0.182.191 and 3.2.0-149.196.
# https://www.openwall.com/lists/oss-security/2020/10/13/7
# https://git.kernel.org/linus/d9ea761fdd197351890418acd462c51f241014a7
#
# git describe --contains d9ea761fdd197351890418acd462c51f241014a7
# v5.15-rc2~29^2~45
CVE_STATUS[CVE-2020-16119] = "fixed-version: Fixed from version v5.15-rc2"


# linux-yocto-custom CVE-2020-1749     5.0      7.5      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2020-1749
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2020-1749
# break-fix: - 6c8991f41546c3c472503dff1ea9daaddf9331c2
# upstream_linux: released (5.5~rc1, 4.4.224, 5.4.5)
#
# git describe --contains 6c8991f41546c3c472503dff1ea9daaddf9331c2
# v5.5-rc1~1^2~38^2
CVE_STATUS[CVE-2020-1749] = "fixed-version: Fixed from version v5.5-rc1"


# linux-yocto-custom CVE-2020-25672    5.0      7.5      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2020-25672
# 
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2020-25672
# break-fix: d646960f7986fefb460a2b062d5ccc8ccfeacc3a 7574fcdbdcb335763b6b322f6928dc0fd5730451
# upstream_linux: released (5.12~rc7, 4.4.267, 5.4.112)
#
# git describe --contains 7574fcdbdcb335763b6b322f6928dc0fd5730451
# v5.12-rc7~13^2~71^2~1
CVE_STATUS[CVE-2020-25672] = "fixed-version: Fixed from version v5.12-rc7"

# linux-yocto-custom CVE-2020-27815    6.1      7.8      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2020-27815
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2020-27815
# break-fix: - c61b3e4839007668360ed8b87d7da96d2e59fc6c
# upstream_linux: released (5.11~rc1, 4.4.249, 5.4.86)
#
# git describe --contains c61b3e4839007668360ed8b87d7da96d2e59fc6c
# v5.11-rc1~142^2
CVE_STATUS[CVE-2020-27815] = "fixed-version: Fixed from version v5.11-rc1"

# linux-yocto-custom CVE-2020-8834     4.9      6.5      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2020-8834
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2020-8834
# break-fix: f024ee098476a3e620232e4a78cfac505f121245 7b0e827c6970e8ca77c60ae87592204c39e41245
# break-fix: f024ee098476a3e620232e4a78cfac505f121245 009c872a8bc4d38f487a9bd62423d019e4322517
# break-fix: f024ee098476a3e620232e4a78cfac505f121245 6f597c6b63b6f3675914b5ec8fcd008a58678650
# upstream_linux: released (4.18~rc1)
#
# git describe --contains 7b0e827c6970e8ca77c60ae87592204c39e41245
# v4.18-rc1~12^2~1^2~33
# git describe --contains 009c872a8bc4d38f487a9bd62423d019e4322517
# v4.18-rc1~12^2~1^2~31
# git describe --contains 6f597c6b63b6f3675914b5ec8fcd008a58678650
# v4.18-rc1~12^2~1^2~30
CVE_STATUS[CVE-2020-8834] = "fixed-version: Fixed from version v4.18-rc1"


# linux-yocto-custom CVE-2021-20194    4.6      7.8      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2021-20194
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2021-20194
# break-fix: 0d01da6afc5402f60325c5da31b22f7d56689b49 bb8b81e396f7afbe7c50d789e2107512274d2a35
# break-fix: 0d01da6afc5402f60325c5da31b22f7d56689b49 f4a2da755a7e1f5d845c52aee71336cee289935a
# upstream_linux: released (5.11~rc7, 5.4.97)
#
# git describe --contains bb8b81e396f7afbe7c50d789e2107512274d2a35
# v5.11-rc7~38^2~21^2~4
# git describe --contains f4a2da755a7e1f5d845c52aee71336cee289935a
# v5.11-rc7~38^2~21^2~3
CVE_STATUS[CVE-2021-20194] = "fixed-version: Fixed from version v5.11-rc7"

# linux-yocto-custom CVE-2021-20265    4.9      5.5      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2021-20265
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2021-20265
# does not exist
#
# https://ubuntu.com/security/CVE-2021-20265
# break-fix: b3ca9b02b00704053a38bfe4c31dbbb9c13595d0 - fa0dc04df259ba2df3ce1920e9690c7842f8fa4b
#
# https://security-tracker.debian.org/tracker/CVE-2021-20265
# https://git.kernel.org/linus/fa0dc04df259ba2df3ce1920e9690c7842f8fa4b (4.5-rc3)
#
# git describe --contains fa0dc04df259ba2df3ce1920e9690c7842f8fa4b
# v4.5-rc3~21^2~48
CVE_STATUS[CVE-2021-20265] = "fixed-version: Fixed from version v4.5-rc3"


# linux-yocto-custom CVE-2021-3564     2.1      5.5      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2021-3564
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2021-3564
# break-fix: - 6a137caec23aeb9e036cdfd8a46dd8a366460e5d
# upstream_linux: released (5.13~rc5, 4.4.272, 5.4.125)
# 
# git describe --contains 6a137caec23aeb9e036cdfd8a46dd8a366460e5d
# v5.13-rc5~9^2~4^2~3
CVE_STATUS[CVE-2021-3564] = "fixed-version: Fixed from version v5.13-rc5"

# linux-yocto-custom CVE-2021-3669     0.0      5.5      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2021-3669
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2021-3669
# break-fix: 7ca7e564e049d8b350ec9d958ff25eaa24226352 20401d1058f3f841f35a594ac2fc1293710e55b9
# upstream_linux: released (5.15~rc1, 5.4.283)
#
# git describe --contains 20401d1058f3f841f35a594ac2fc1293710e55b9
# v5.15-rc1~58^2~2
CVE_STATUS[CVE-2021-3669] = "fixed-version: Fixed from version v5.15-rc1"


# linux-yocto-custom CVE-2021-3714     0.0      5.9      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2021-3714
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2021-3714
# upstream_linux: deferred (2024-06-17)
#
# https://ubuntu.com/security/CVE-2021-3714
# there is no upstream fix available as of 2024-06-17
#
# https://security-tracker.debian.org/tracker/CVE-2021-3714
# A flaw was found in the Linux kernels memory deduplication mechanism.
# Previous work has shown that memory deduplication can be attacked via a local exploitation mechanism.
# The same technique can be used if an attacker can upload page sized files and detect the change in access time from a networked service to determine if the page has been merged.
# vulnerable/unfixed/unimportant
# https://bugzilla.redhat.com/show_bug.cgi?id=1931327
# Inherent design limitation, can be avoided by not using KSM
# CONFIG_KSM is not set
CVE_STATUS[CVE-2021-3714] = "not-applicable-config: CONFIG_KSM is not set"


# linux-yocto-custom CVE-2021-3759     0.0      5.5      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2021-3759
#  
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2021-3759
# break-fix: a9bb7e620efdfd29b6d1c238041173e411670996 18319498fdd4cdf8c1c2c48cd432863b1f915d6f
# upstream_linux: released (5.15~rc1, 5.4.224)
# 
# git describe --contains 18319498fdd4cdf8c1c2c48cd432863b1f915d6f
# v5.15-rc1~107^2~104
CVE_STATUS[CVE-2021-3759] = "fixed-version: Fixed from version v5.15-rc1"


# linux-yocto-custom CVE-2021-3864     0.0      7.0      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2021-3864
# @@@TODO: mitigations
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2021-3864
# upstream_linux: needs-triage
#
# https://ubuntu.com/security/CVE-2021-3864
# no fix upstream as of 2022-01-27
# 
# https://security-tracker.debian.org/tracker/CVE-2021-3864
# vulnerable/unfixed
# A flaw was found in the way the dumpable flag setting was handled when certain SUID binaries executed its descendants.
# The prerequisite is a SUID binary that sets real UID equal to effective UID, and real GID equal to effective GID.
# The descendant will then have a dumpable value set to 1.
# As a result, if the descendant process crashes and core_pattern is set to a relative value, its core dump is stored in the current directory with uid:gid permissions.
# An unprivileged local user with eligible root SUID binary could use this flaw to place core dumps into root-owned directories, potentially resulting in escalation of privileges.
#
# https://www.openwall.com/lists/oss-security/2021/10/20/2
#
# Workarounds:
# ---------------------------------------------------------
# This exploit can be mitigated using a few workarounds.
#
# 1. Change /proc/sys/kernel/core_pattern to an absolute, safe directory
# (this way, logrotate cannot be triggered).
# 2. Apparently, at some point in time, pam_limits.so was removed from the
# PAM configuration of sudo (in Debian and Ubuntu). Adding this module to the
# sudo's PAM configuration and setting RLIMIT_CORE to 0 makes sudo immune to
# this vulnerability.
# 
#
# As discussed with security@...nel.org and linux-distros@...openwall.org a
# permanent fix to this vulnerability will probably need to be inside the
# linux kernel, one of the suggestions that came up was to reset RLIMIT_CORE
# of a suid process to 0 by default. Any fix to this issue would probably
# break user expectations from the linux kernel because suddenly their
# coredump won't be created.
#
# discussions on the LKML:
# https://lore.kernel.org/all/20211221021744.864115-1-longman@redhat.com/T/#u
#
CVE_STATUS[CVE-2021-3864] = "not-applicable-config: core dumps are not enabled by default, see above mitigations"


# linux-yocto-custom CVE-2021-4218     0.0      5.5      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2021-4218
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2021-4218
# does not exist
#
# https://ubuntu.com/security/CVE-2021-4218
# break - fix: df971cd853c05778ae1175e8aeb80a04bb9d4be5 - 32927393dc1ccd60fb2bdc05b9e8e88753761469
#
# https://security-tracker.debian.org/tracker/CVE-2021-4218
# A flaw was found in the Linux kernel’s implementation of reading the SVC RDMA counters.
# Reading the counter sysctl panics the system. 
# This flaw allows a local attacker with local access to cause a denial of service while the system reboots.
# The issue is specific to CentOS/RHEL.
# - linux <not-affected> (Vulnerable code not present; specific to CentOS/RHEL)
# https://bugzilla.redhat.com/show_bug.cgi?id=2048359
# Issue is specific to CentOS/RHEL. In mainline, xprtrdma always used copy_to_user()
# until the general conversion of sysctls to use a kernel buffer.
# 
# git describe --contains 32927393dc1ccd60fb2bdc05b9e8e88753761469
# v5.8-rc1~55^2~6
CVE_STATUS[CVE-2021-4218] = "fixed-version: Fixed from version v5.8-rc1"
#CVE_STATUS[CVE-2021-4218] = "not-applicable-platform: Issue only applies on CentOS/RHEL"

# linux-yocto-custom CVE-2022-0286     2.1      5.5      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2022-0286
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2022-0286
# break-fix: 18cb261afd7bf50134e5ccacc5ec91ea16efadd4 105cd17a866017b45f3c45901b394c711c97bf40
# upstream_linux: released (5.14~rc2)
# 
# git describe --contains 105cd17a866017b45f3c45901b394c711c97bf40
# v5.14-rc2~28^2~41^2~7
CVE_STATUS[CVE-2022-0286] = "fixed-version: Fixed from version v5.14-rc2"


# linux-yocto-custom CVE-2022-0400     0.0      7.5      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2022-0400
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2022-0400
# upstream_linux: deferred (2024-06-17)
#
# https://ubuntu.com/security/CVE-2022-0400
# no fix available as of 2024-06-17
#
# https://security-tracker.debian.org/tracker/CVE-2022-0400
# An out-of-bounds read vulnerability was discovered in linux kernel in the smc protocol stack, causing remote dos.
# vulnerable/unfixed/unimportant
# [stretch] - linux <not-affected> (Vulnerable code not present)
# https://bugzilla.redhat.com/show_bug.cgi?id=2044575
# https://bugzilla.redhat.com/show_bug.cgi?id=2040604 (not public)
# non issue, no security impact
#
# Symbol: SMC [=n]
#  │ Type  : tristate
#  │ Defined at net/smc/Kconfig:2
#  │   Prompt: SMC socket protocol family
#  │   Depends on: NET [=y] && INET [=y] && INFINIBAND [=n] && (m [=m] && MODULES [=y] || ISM [=n]!=m [=m])
#  │   Location:
#  │     -> Networking support (NET [=y])
#  │ (1)   -> Networking options
#  │ -> SMC socket protocol family (SMC [=n])
#  │ Implied by [n]:
#  │   - ISM [=n] && NETDEVICES [=y] && S390 && PCI [=y]
CVE_STATUS[CVE-2022-0400] = "not-applicable-config: CONFIG_SMC is not set"


# linux-yocto-custom CVE-2022-1247     0.0      7.0      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2022-1247
# 
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2022-1247
# upstream_linux: needs-triage
#
# https://ubuntu.com/security/CVE-2022-1247
# mitigation is to disable and prevent af_rose from being autoloaded, I suspect. unfixed upstream as of 2023.01.10
#
# https://security-tracker.debian.org/tracker/CVE-2022-1247
# vulnerable/unfixed/unimportant
# https://bugzilla.redhat.com/show_bug.cgi?id=2066799
# Mitigated by hamradio-disable-auto-loading-as-mitigation-against-local-exploits.patch
#
# if really needed we could apply this patch:
# https://sources.debian.org/patches/linux/6.11.2-1/debian/hamradio-disable-auto-loading-as-mitigation-against-local-exploits.patch/
# net/ax25/af_ax25.c
# net/netrom/af_netrom.c
# net/rose/af_rose.c
# 
# CONFIG_HAMRADIO is not set
CVE_STATUS[CVE-2022-1247] = "not-applicable-config: CONFIG_HAMRADIO is not set"
 


# linux-yocto-custom CVE-2022-1462     3.3      6.3      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2022-1462
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2022-1462
# break-fix: - b6da31b2c07c46f2dcad1d86caa835227a16d9ff
# break-fix: b6da31b2c07c46f2dcad1d86caa835227a16d9ff 71a174b39f10b4b93223d374722aa894b5d8a82e
# break-fix: b6da31b2c07c46f2dcad1d86caa835227a16d9ff 6b9dbedbe3499fef862c4dff5217cf91f34e43b3
# break-fix: 71a174b39f10b4b93223d374722aa894b5d8a82e a501ab75e7624d133a5a3c7ec010687c8b961d23
# upstream_linux: released (5.19~rc7, 5.4.208, 5.15.58)
#
# git describe --contains 71a174b39f10b4b93223d374722aa894b5d8a82e
# v5.10-rc1~129^2~33
# git describe --contains 6b9dbedbe3499fef862c4dff5217cf91f34e43b3
# v5.19-rc1~121^2~11
# git describe --contains a501ab75e7624d133a5a3c7ec010687c8b961d23
# v5.19-rc7~12^2
CVE_STATUS[CVE-2022-1462] = "fixed-version: Fixed from version v5.19-rc7"


# linux-yocto-custom CVE-2022-2308     0.0      6.5      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2022-2308
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2022-2308
#
# break-fix: c8a6153b6c59d95c0e091f053f6f180952ade91e 46f8a29272e51b6df7393d58fc5cb8967397ef2b
# upstream_linux: released (6.0, 5.15.72)
#
# git describe --contains 46f8a29272e51b6df7393d58fc5cb8967397ef2b
# v6.0~11^2~1
CVE_STATUS[CVE-2022-2308] = "fixed-version: Fixed from version v6.0"


# linux-yocto-custom CVE-2022-2327     0.0      7.8      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2022-2327
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2022-2327
# does not exist
#
# vendor advisory/patch:
# https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git/commit/?h=linux-5.10.y&id=df3f3bb5059d20ef094d6b2f0256c4bf4127a859
# https://kernel.dance/#df3f3bb5059d20ef094d6b2f0256c4bf4127a859
#
# git describe --contains df3f3bb5059d20ef094d6b2f0256c4bf4127a859
# v5.10.125~1
CVE_STATUS[CVE-2022-2327] = "fixed-version: Fixed from version v5.10.125"


# linux-yocto-custom CVE-2022-2663     0.0      5.3      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2022-2663
# 
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2022-2663
# break-fix: 869f37d8e48f3911eb70f38a994feaa8f8380008 0efe125cfb99e6773a7434f3463f7c2fa28f3a43
# break-fix: 869f37d8e48f3911eb70f38a994feaa8f8380008 e8d5dfd1d8747b56077d02664a8838c71ced948e
# upstream_linux: released (6.0~rc7, 5.4.215, 5.15.71)
# 
# git describe --contains 0efe125cfb99e6773a7434f3463f7c2fa28f3a43
# v6.0-rc5~27^2~22^2
# git describe --contains e8d5dfd1d8747b56077d02664a8838c71ced948e
# v6.0-rc7~24^2~54^2~1
CVE_STATUS[CVE-2022-2663] = "fixed-version: Fixed from version v6.0-rc7"

# linux-yocto-custom CVE-2022-2785     0.0      5.5      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2022-2785
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2022-2785
# does not exist
# 
# patch/vendor advisory
# https://git.kernel.org/pub/scm/linux/kernel/git/bpf/bpf.git/commit/?id=86f44fcec22c
# fix: 86f44fcec22ce2979507742bc53db8400e454f46
# 
# git describe --contains 86f44fcec22ce2979507742bc53db8400e454f46
# v6.0-rc1~28^2~14^2~1
CVE_STATUS[CVE-2022-2785] = "fixed-version: Fixed from version v6.0-rc1"


# linux-yocto-custom CVE-2022-3435     0.0      4.3      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2022-3435
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2022-3435
# break-fix: 6bf92d70e690b7ff12b24f4bfff5e5434d019b82 61b91eb33a69c3be11b259c5ea484505cd79f883
# break-fix: 493ced1ac47c48bb86d9d4e8e87df8592be85a0e 61b91eb33a69c3be11b259c5ea484505cd79f883
# upstream_linux: released (6.1~rc1, 5.4.226, 5.15.82)
#
# git describe --contains 61b91eb33a69c3be11b259c5ea484505cd79f883
# v6.1-rc1~33^2~30
CVE_STATUS[CVE-2022-3435] = "fixed-version: Fixed from version v6.1-rc1"


# linux-yocto-custom CVE-2022-3523     0.0      5.3      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2022-3523
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2022-3523
# break-fix: 5042db43cc26f51eed51c56192e2c2317e44315f 16ce101db85db694a91380aa4c89b25530871d33
# break-fix: 27674ef6c73f0c9096a9827dc5d6ba9fc7808422 0dc45ca1ce18900572282c4f054bbe78351cb6a7
# upstream_linux: released (6.1~rc1)
# 
# git describe --contains 16ce101db85db694a91380aa4c89b25530871d33
# v6.1-rc1~21^2~16
# git describe --contains 0dc45ca1ce18900572282c4f054bbe78351cb6a7
# v6.1-rc1~21^2~14
CVE_STATUS[CVE-2022-3523] = "fixed-version: Fixed from version v6.1-rc1"

# linux-yocto-custom CVE-2022-3534     0.0      8.0      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2022-3534
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2022-3534
# upstream: https://github.com/libbpf/libbpf/commit/54caf920db0e489de90f3aaaa41e2a51ddbcd084
# upstream_libbpf: needs-triage
# 
# advisory/patch: 
# https://git.kernel.org/pub/scm/linux/kernel/git/bpf/bpf-next.git/commit/?id=93c660ca40b5d2f7c1b1626e955a8e9fa30e0749
#
# git describe --contains 93c660ca40b5d2f7c1b1626e955a8e9fa30e0749
# v6.2-rc1~99^2~413^2~4^2~5
CVE_STATUS[CVE-2022-3534] = "fixed-version: Fixed from version v6.2-rc1"



# linux-yocto-custom CVE-2022-3566     0.0      7.1      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2022-3566
# 
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2022-3566
# break-fix: 1da177e4c3f41524e886b7f1b8a0c1fc7321cac2 f49cd2f4d6170d27a2c61f1fecb03d8a70c91f57
# upstream_linux: released (6.1~rc1, 5.4.279, 5.15.162)
#
# git describe --contains f49cd2f4d6170d27a2c61f1fecb03d8a70c91f57
# v6.1-rc1~33^2~7^2
CVE_STATUS[CVE-2022-3566] = "fixed-version: Fixed from version v6.1-rc1"


# linux-yocto-custom CVE-2022-3567     0.0      6.4      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2022-3567
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2022-3567
# break-fix: 1da177e4c3f41524e886b7f1b8a0c1fc7321cac2 364f997b5cfe1db0d63a390fe7c801fa2b3115f6
# upstream_linux: released (6.1~rc1, 5.4.279, 5.15.162)
# 
# git describe --contains 364f997b5cfe1db0d63a390fe7c801fa2b3115f6
# v6.1-rc1~33^2~7^2~1
CVE_STATUS[CVE-2022-3567] = "fixed-version: Fixed from version v6.1-rc1"

# linux-yocto-custom CVE-2022-3619     0.0      4.3      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2022-3619
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2022-3619
# break-fix: 4d7ea8ee90e42fc75995f6fb24032d3233314528 7c9524d929648935bac2bbb4c20437df8f9c3f42
# upstream_linux: released (6.1~rc4, 5.15.78)
#
# git describe --contains 7c9524d929648935bac2bbb4c20437df8f9c3f42
# v6.1-rc4~26^2~9^2~4
CVE_STATUS[CVE-2022-3619] = "fixed-version: Fixed from version v6.1-rc4"

# linux-yocto-custom CVE-2022-3621     0.0      6.5      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2022-3621
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2022-3621
# break-fix: 05fe58fdc10df9ebea04c0eaed57adc47af5c184 21a87d88c2253350e115029f14fe2a10a7e6c856
# upstream_linux: released (6.1~rc1, 5.4.218, 5.15.74)
#
# git describe --contains 21a87d88c2253350e115029f14fe2a10a7e6c856
# v6.1-rc1~44^2~1
CVE_STATUS[CVE-2022-3621] = "fixed-version: Fixed from version v6.1-rc1"

# linux-yocto-custom CVE-2022-3624     0.0      3.3      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2022-3624
# 
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2022-3624
# !!! does not exist
#
# vendor advisory:
# https://git.kernel.org/pub/scm/linux/kernel/git/klassert/ipsec-next.git/commit/?id=4f5d33f4f798b1c6d92b613f0087f639d9836971
# 
# git describe --contains 4f5d33f4f798b1c6d92b613f0087f639d9836971
# v6.0-rc1~28^2~6
CVE_STATUS[CVE-2022-3624] = "fixed-version: Fixed from version v6.0-rc1"

# linux-yocto-custom CVE-2022-3629     1.4      3.3      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2022-3629
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2022-3629
# break-fix: d021c344051af91f42c5ba9fdedc176740cbd238 7e97cfed9929eaabc41829c395eb0d1350fccb9d
# upstream_linux: released (6.0~rc1, 5.4.211, 5.15.63)
#
# git describe --contains 7e97cfed9929eaabc41829c395eb0d1350fccb9d
# v6.0-rc1~28^2~21
CVE_STATUS[CVE-2022-3629] = "fixed-version: Fixed from version v6.0-rc1"


# linux-yocto-custom CVE-2022-3630     0.0      5.5      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2022-3630
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2022-3630
# !!! does not exist
#
# https://git.kernel.org/pub/scm/linux/kernel/git/klassert/ipsec-next.git/commit/?id=fb24771faf72a2fd62b3b6287af3c610c3ec9cf1
# 
# git describe --contains fb24771faf72a2fd62b3b6287af3c610c3ec9cf1
# v6.0-rc1~48^2~1
CVE_STATUS[CVE-2022-3630] = "fixed-version: Fixed from version v6.0-rc1"

# linux-yocto-custom CVE-2022-3633     2.7      3.3      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2022-3633
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2022-3633
# break-fix: 9d71dd0c70099914fcd063135da3c580865e924c 8c21c54a53ab21842f5050fa090f26b03c0313d6
# upstream_linux: released (6.0~rc1, 5.4.211, 5.15.63)
#
# git describe --contains 8c21c54a53ab21842f5050fa090f26b03c0313d6
# v6.0-rc1~28^2~24^2~2
CVE_STATUS[CVE-2022-3633] = "fixed-version: Fixed from version v6.0-rc1"


# linux-yocto-custom CVE-2022-3636     0.0      7.8      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2022-3636
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2022-3636
# !!! does not exist 
# 
# https://nvd.nist.gov/vuln/detail/CVE-2022-3636
# vendor advisory:
# https://git.kernel.org/pub/scm/linux/kernel/git/pabeni/net-next.git/commit/?id=17a5f6a78dc7b8db385de346092d7d9f9dc24df6
#
# git describe --contains 17a5f6a78dc7b8db385de346092d7d9f9dc24df6
# v5.19-rc1~159^2~325
CVE_STATUS[CVE-2022-3636] = "fixed-version: Fixed from version v5.19-rc1"




# linux-yocto-custom CVE-2022-36402    0.0      5.5      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2022-36402
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2022-36402
# break-fix: d80efd5cb3dec16a8d1aea9b8a4a7921972dba65 14abdfae508228a7307f7491b5c4215ae70c6542
# upstream_linux: released (6.5, 5.15.129)
# 
# git describe --contains 14abdfae508228a7307f7491b5c4215ae70c6542
# v6.5~12^2~1^2~1
CVE_STATUS[CVE-2022-36402] = "fixed-version: Fixed from version v6.5"


# linux-yocto-custom CVE-2022-3646     0.0      4.3      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2022-3646
# 
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2022-3646
# break-fix: 9ff05123e3bfbb1d2b68ba1d9bf1f7d1dffc1453 d0d51a97063db4704a5ef6bc978dddab1636a306
# upstream_linux: released (6.1~rc1, 5.4.218, 5.15.74)
#
# git describe --contains d0d51a97063db4704a5ef6bc978dddab1636a306
# v6.1-rc1~44^2
CVE_STATUS[CVE-2022-3646] = "fixed-version: Fixed from version v6.1-rc1"



# linux-yocto-custom CVE-2022-38096    0.0      5.5      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2022-38096
# 
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2022-38096
# break-fix: 9c079b8ce8bf8e0394149eb39c78b04285644bcc 517621b7060096e48e42f545fa6646fc00252eac
# upstream_linux: released (6.9~rc1, 5.15.154, 6.8.3)
# 
# git describe --contains 517621b7060096e48e42f545fa6646fc00252eac
# v6.9-rc1~26^2~27^2~51
CVE_STATUS[CVE-2022-38096] = "fixed-version: Fixed from version v6.9-rc1"


# linux-yocto-custom CVE-2022-42895    0.0      6.5      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2022-42895
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2022-42895
# break-fix: 42dceae2819b5ac6fc9a0d414ae05a8960e2a1d9 b1a2cd50c0357f243b7435a732b4e62ba3157a2e
# upstream_linux: released (6.1~rc4, 5.4.224, 5.15.78)
#
# git describe --contains b1a2cd50c0357f243b7435a732b4e62ba3157a2e
# v6.1-rc4~26^2~9^2
CVE_STATUS[CVE-2022-42895] = "fixed-version: Fixed from version v6.1-rc4"

# linux-yocto-custom CVE-2022-4382     0.0      6.4      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2022-4382
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2022-4382
# break-fix: e5d82a7360d124ae1a38c2a5eac92ba49b125191 d18dcfe9860e842f394e37ba01ca9440ab2178f4
# upstream_linux: released (6.2~rc5, 5.4.230, 5.15.90)
#
# git describe --contains d18dcfe9860e842f394e37ba01ca9440ab2178f4
# v6.2-rc5~6^2~28
CVE_STATUS[CVE-2022-4382] = "fixed-version: Fixed from version v6.2-rc5"

# linux-yocto-custom CVE-2022-4543     0.0      5.5      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2022-4543
#
# https://bugzilla.redhat.com/show_bug.cgi?id=2153871
# KPTI has fundamental design flaws, allowing any local attacker to easily, quickly, and reliably leak KASLR base via prefetch side-channels based on TLB timing for Intel systems.
#
# https://ubuntu.com/security/CVE-2022-4543
# unfixed upstream as of 2023.01.10
#
# https://security-tracker.debian.org/tracker/CVE-2022-4543
# A flaw named "EntryBleed" was found in the Linux Kernel Page Table Isolation (KPTI).
# This issue could allow a local attacker to leak KASLR base via prefetch side-channels based on TLB timing for Intel systems.
# vulnerable/unfixed
# [bookworm] - linux <postponed> (Minor issue, revisit when/if fixed upstream)
# [bullseye] - linux <postponed> (Minor issue, revisit when/if fixed upstream)
# https://www.openwall.com/lists/oss-security/2022/12/16/3
# https://www.willsroot.io/2022/12/entrybleed.html
#
# This is only for x86 Architecture and
# CONFIG_PAGE_TABLE_ISOLATION does not exist in the arm configuration
CVE_STATUS[CVE-2022-4543] = "not-applicable-config: CONFIG_PAGE_TABLE_ISOLATION does not exist for arm"

# linux-yocto-custom CVE-2023-1073     0.0      6.6      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2023-1073
# 
# https://ubuntu.com/security/CVE-2023-1073
# linux	Introduced by 1b15d2e5b8077670b1e6a33250a0d9577efff4a5, fixed by b12fece4c64857e5fab4290bf01b2e0317a88456
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2023-1073
# break-fix: 1b15d2e5b8077670b1e6a33250a0d9577efff4a5 b12fece4c64857e5fab4290bf01b2e0317a88456
# upstream_linux: released (6.2~rc5, 5.4.231, 5.15.91)
#
# git describe --contains b12fece4c64857e5fab4290bf01b2e0317a88456
# v6.2-rc5~30^2~6
CVE_STATUS[CVE-2023-1073] = "fixed-version: Fixed from version v6.2-rc5"

# linux-yocto-custom CVE-2023-1074     0.0      5.5      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2023-1074
# 
# git describe --contains https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2023-1074
# break-fix: 1da177e4c3f41524e886b7f1b8a0c1fc7321cac2 458e279f861d3f61796894cd158b780765a1569f
# upstream_linux: released (6.2~rc6, 5.4.231, 5.15.91)
#
# git describe --contains 458e279f861d3f61796894cd158b780765a1569f
# v6.2-rc6~25^2~9
CVE_STATUS[CVE-2023-1074] = "fixed-version: Fixed from version v6.2-rc6"

# linux-yocto-custom CVE-2023-1075     0.0      3.3      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2023-1075
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2023-1075
# Patches_linux:
# break-fix: a42055e8d2c30d4decfc13ce943d09c7b9dad221 ffe2a22562444720b05bdfeb999c03e810d84cbb
# upstream_linux: released (6.2~rc7)
#
# git describe --contains ffe2a22562444720b05bdfeb999c03e810d84cbb
# v6.2-rc7~20^2~23
CVE_STATUS[CVE-2023-1075] = "fixed-version: Fixed from version v6.2-rc7"


# linux-yocto-custom CVE-2023-1076     0.0      5.5      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2023-1076
#
# https://ubuntu.com/security/CVE-2023-1076
# 
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2023-1076
# Patches_linux:
# break-fix: 86741ec25462e4c8cdce6df2f41ead05568c7d5e a096ccca6e503a5c575717ff8a36ace27510ab0a
# break-fix: 86741ec25462e4c8cdce6df2f41ead05568c7d5e 66b2c338adce580dfce2199591e65e2bab889cff
# upstream_linux: released (6.3~rc1, 5.4.235, 5.15.99)
#
# git describe --contains a096ccca6e503a5c575717ff8a36ace27510ab0a
# v6.3-rc1~162^2~130^2~1
CVE_STATUS[CVE-2023-1076] = "fixed-version: Fixed from version v6.3-rc1"

# linux-yocto-custom CVE-2023-2898     0.0      4.7      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2023-2898
#
# https://ubuntu.com/security/CVE-2023-2898
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2023-2898
# break-fix: b4b10061ef98c583bcf82a4200703fbaa98c18dc d8189834d4348ae608083e1f1f53792cfcc2a9bc
# upstream_linux: released (6.5~rc1, 5.15.121)
#
# git describe --contains d8189834d4348ae608083e1f1f53792cfcc2a9bc
# v6.5-rc1~43^2~31
CVE_STATUS[CVE-2023-2898] = "fixed-version: Fixed from version v6.5-rc1"



# linux-yocto-custom CVE-2023-3397     0.0      6.3      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2023-3397
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2023-3397
#
# https://ubuntu.com/security/CVE-2023-3397
# unfixed upstream as of 2023-09-01
#
# https://security-tracker.debian.org/tracker/CVE-2023-3397
# https://lore.kernel.org/lkml/20230515095956.17898-1-zyytlz.wz@163.com/
# CONFIG_JFS_FS is not set
CVE_STATUS[CVE-2023-3397] = "not-applicable-config: CONFIG_JFS_FS is not set"

# linux-yocto-custom CVE-2023-3640     0.0      7.8      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2023-3640
#
# https://wiki.yoctoproject.org/wiki/CVE_Status#CVE-2023-3640_(linux-yocto)
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2023-3640
# Description:
# A possible unauthorized memory access flaw was found in the Linux kernel's
# cpu_entry_area mapping of X86 CPU data to memory, where a user may guess
# the location of exception stacks or other important data. Based on the
# previous CVE-2023-0597, the 'Randomize per-cpu entry area' feature was
# implemented in /arch/x86/mm/cpu_entry_area.c, which works through the
# init_cea_offsets() function when KASLR is enabled. However, despite this
# feature, there is still a risk of per-cpu entry area leaks. This issue
# could allow a local user to gain access to some important data with memory
# in an expected location and potentially escalate their privileges on the
# system.
#
# https://ubuntu.com/security/CVE-2023-3640
# appears unfixed upstream as of 2023-09-01
# 
# https://security-tracker.debian.org/tracker/CVE-2023-3640
# vulnerable/unfixed - unimportant
# 
# Essentially, this is a CPU-level address leak vulnerability
# that affects the vast majority of Intel CPUs (possibly AMD CPUs as well),
# due to the inability to resolve the issues introduced by KPTI (Kernel Page
# Table Isolation). 
CVE_STATUS[CVE-2023-3640] = "not-applicable-config: this only affects Intel CPUs (possibly AMD CPUs as well)"

# linux-yocto-custom CVE-2023-3772     0.0      4.4      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2023-3772
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2023-3772
# break-fix: d8647b79c3b7e223ac051439d165bc8e7bbb832f 00374d9b6d9f932802b55181be9831aa948e5b7c
# upstream_linux: released (6.5~rc7, 5.4.255, 5.15.128)
# 
# git describe --contains 00374d9b6d9f932802b55181be9831aa948e5b7c
# v6.5-rc7~19^2~12^2~3
CVE_STATUS[CVE-2023-3772] = "fixed-version: Fixed from version v6.5-rc7"


# linux-yocto-custom CVE-2023-3773     0.0      4.4      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2023-3773
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2023-3773
# break-fix: 4e484b3e969b52effd95c17f7a86f39208b2ccf4 5e2424708da7207087934c5c75211e8584d553a0
# upstream_linux: released (6.5~rc7, 5.15.128)
#
# git describe --contains 5e2424708da7207087934c5c75211e8584d553a0
# v6.5-rc7~19^2~12^2~2
CVE_STATUS[CVE-2023-3773] = "fixed-version: Fixed from version v6.5-rc7"

# linux-yocto-custom CVE-2023-4010     0.0      4.6      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2023-4010
#
# https://wiki.yoctoproject.org/wiki/CVE_Status#CVE-2023-4010_(linux-yocto)
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2023-4010
# Patches_linux:
# break-fix: 21677cfc562a27e099719d413287bc8d1d24deb7 -
# upstream_linux: needed 
#
# https://ubuntu.com/security/CVE-2023-4010
# break-fix: 21677cfc562a27e099719d413287bc8d1d24deb7 -
# sbeattie
# issue is not clear; the function the reporter mentions does not exist.
# though there is the similar usb_giveback_urb_bh().
# However, as of 6.3.7, which the reporter claims is vulnerable, that function contained no gotos,
# though it did until 26c6c2f8a907 (“USB: HCD: Fix URB giveback issue in tasklet function”), which landed in v6.0-rc1.
#
# magalilemes
# This is a USB device using interrupt transfers.
# So, as soon as a response is received, it submits another URB, waiting for the next “interrupt” to happen.
# That is totally normal. 
# But as the response is malformed, the imon driver outputs a warning without any throttling.
# There is no system lockup happening.
# The imon driver has been issuing those warnings since its inception, so using that as the break commit.
#
# https://security-tracker.debian.org/tracker/CVE-2023-4010
# vulnerable/unfixed
#
# Priority: low
# Requires physical access to insert a malicious device, and the denial
# of service is simply log flooding due to a malforrmed response.
CVE_STATUS[CVE-2023-4010] = "ignored: cpe:*:linux_kernel:requires physical access to insert a malicious USB device, and the denial of service is simply log flooding due to a malforrmed response."

# linux-yocto-custom CVE-2023-4155     0.0      5.6      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2023-4155
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2023-4155
# Patches_linux:
# break-fix: 291bd20d5d88814a73d43b55b9428feab2f28094 7588dbcebcbf0193ab5b76987396d0254270b04a
# upstream_linux: released (6.5~rc6)
# 
# git describe --contains 7588dbcebcbf0193ab5b76987396d0254270b04a
# v6.5-rc6~39^2~3
CVE_STATUS[CVE-2023-4155] = "fixed-version: Fixed from version v6.5-rc6"


# linux-yocto-custom CVE-2023-52904    0.0      5.5      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2023-52904
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2023-52904
# break-fix: 291e9da91403e0e628d7692b5ed505100e7b7706 92a9c0ad86d47ff4cce899012e355c400f02cfb8
# upstream_linux: released (2.6.12~rc2, 5.15.168)
#
# git describe --contains 92a9c0ad86d47ff4cce899012e355c400f02cfb8
# v6.2-rc4~18^2~1
CVE_STATUS[CVE-2023-52904] = "fixed-version: Fixed from version v6.2-rc4"

# linux-yocto-custom CVE-2023-6176     0.0      4.7      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2023-6176
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2023-6176
# break-fix: 635d9398178659d8ddba79dd061f9451cec0b4d1 cfaa80c91f6f99b9342b6557f0f0e1143e434066
# upstream_linux: released (6.6~rc2, 5.4.257, 5.15.132)
#
# https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=cfaa80c91f6f99b9342b6557f0f0e1143e434066
# 
# git describe --contains cfaa80c91f6f99b9342b6557f0f0e1143e434066
# v6.6-rc2~29^2~8
CVE_STATUS[CVE-2023-6176] = "fixed-version: Fixed from version v6.6-rc2"

# linux-yocto-custom CVE-2023-6238     0.0      6.7      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2023-6238
# 
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2023-6238
# break-fix: 855b7717f44b13e0990aa5ad36bbf9aa35051516  - this means only break and no fix?
# git describe --contains 855b7717f44b13e0990aa5ad36bbf9aa35051516
# v6.2-rc1~129^2~46^2~40
# not fixed upstream
#  
# https://ubuntu.com/security/CVE-2023-6238
# unfixed upstream as of 2024-04-10
# 
# I could argue, that I don't support NVME in my config
# NVME Support
#
# CONFIG_BLK_DEV_NVME is not set
# CONFIG_NVME_FC is not set
# CONFIG_NVME_TCP is not set
# CONFIG_NVME_TARGET is not set
# CONFIG_NVME_TARGET_TCP is not set
# end of NVME Support
CVE_STATUS[CVE-2023-6238] = "not-applicable-config: CONFIG_BLK_DEV_NVME, CONFIG_NVME_FC, CONFIG_NVME_TCP, CONFIG_NVME_TARGET, CONFIG_NVME_TARGET_TCP are not set"


# linux-yocto-custom CVE-2023-6240     0.0      6.5      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2023-6240
#
# https://ubuntu.com/security/CVE-2023-6240
# appears unfixed in upstream as of 2024.08.24
# 
# https://security.access.redhat.com/data/csaf/v2/advisories/2024/rhsa-2024_1881.json
# {
#         "category": "other",
#         "text": "Red Hat Enterprise Linux 6 and 7 are not affected by this CVE as they did not include kernel TLS support (upstream commit 3c4d755). Red Hat Enterprise Linux 8 is not affected as it did not include the upstream commit that introduced this flaw (fd31f39 \"tls: rx: decrypt into a fresh skb\").",
#         "title": "Statement"
#       },
#
# git show 3c4d7559159bfe1e3b94df3a657b2cda3a34e218
# git tag --contains 3c4d7559159bfe1e3b94df3a657b2cda3a34e218
#
# more explanation:
# https://people.redhat.com/~hkario/marvin/
#
# https://osv.dev/vulnerability/CVE-2023-6240
# no fix available
#
# https://security-tracker.debian.org/tracker/CVE-2023-6240
# unfixed
#
# I guess I could argue here, that I do not include TLS support in the kernel
# CONFIG_TLS is not set
CVE_STATUS[CVE-2023-6240] = "not-applicable-config: CONFIG_TLS is not set"

# linux-yocto-custom CVE-2023-6270     0.0      7.0      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2023-6270
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2023-6270
# Patches_linux:
# break-fix: 7562f876cd93800f2f8c89445f2a563590b24e09 f98364e926626c678fb4b9004b75cacf92ff0662
# upstream_linux: released (6.9~rc1, 5.4.273, 5.15.153, 6.8.2)
#
# It is patched here: git show f98364e926626c678fb4b9004b75cacf92ff0662
# git describe --contains f98364e926626c678fb4b9004b75cacf92ff0662
# v6.9-rc1~214^2~7
CVE_STATUS[CVE-2023-6270] = "fixed-version: Fixed from version v6.9-rc1"

# linux-yocto-custom CVE-2023-6535     0.0      7.5      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2023-6535
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2023-6535
# Patches_linux:
# break-fix: 872d26a391da92ed8f0c0f5cb5fef428067b7f30 efa56305908ba20de2104f1b8508c6a7401833be
# break-fix: 872d26a391da92ed8f0c0f5cb5fef428067b7f30 0849a5441358cef02586fb2d60f707c0db195628
# break-fix: efa56305908ba20de2104f1b8508c6a7401833be 9a1abc24850eb759e36a2f8869161c3b7254c904
# upstream_linux: released (6.8~rc1, 5.4.268, 5.15.148)
#
# git describe --contains efa56305908ba20de2104f1b8508c6a7401833be
# v6.8-rc1~28^2~14^2~24
CVE_STATUS[CVE-2023-6535] = "fixed-version: Fixed from version v6.8-rc1"


# linux-yocto-custom CVE-2023-6610     0.0      7.1      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2023-6610
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2023-6610
# break-fix: - 567320c46a60a3c39b69aa1df802d753817a3f86
# upstream_linux: released (6.7~rc7)
#
# git show 567320c46a60a3c39b69aa1df802d753817a3f86
# git describe --contains 567320c46a60a3c39b69aa1df802d753817a3f86
# v6.7-rc7~23^2~3
CVE_STATUS[CVE-2023-6610] = "fixed-version: Fixed from version v6.7-rc7"

# linux-yocto-custom CVE-2023-6679     0.0      5.5      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2023-6679
#
# !!! https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2023-6679
# !!! break-fix: 9d71b54b65b1fb6c0d3a6c5c88ba9b915c783fbc 65c95f78917ea6fa7ff189a2c19879c4fe161873
# !!! upstream_linux: not-affected (debian: Vulnerable code not present)
#
# It is patched here: git show 65c95f78917ea6fa7ff189a2c19879c4fe161873
# git describe --contains 65c95f78917ea6fa7ff189a2c19879c4fe161873
# v6.7-rc6~21^2~11
# git tag --contains 65c95f78917ea6fa7ff189a2c19879c4fe161873
# ...
CVE_STATUS[CVE-2023-6679] = "fixed-version: Fixed from version v6.7-rc6"

# linux-yocto-custom CVE-2023-7042     0.0      5.5      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2023-7042
# Patch applied to ath-next branch of ath.git, thanks.
# 
# ad25ee36f001 wifi: ath10k: fix NULL pointer dereference in ath10k_wmi_tlv_op_pull_mgmt_tx_compl_ev()
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2023-7042
# break-fix: dc405152bb64d4ae01c9ac669de25b2d1fb6fc2d ad25ee36f00172f7d53242dc77c69fff7ced0755
# upstream_linux: released (6.9~rc1, 5.4.273, 5.15.153, 6.8.2)
#
# It is patched here: git show ad25ee36f00172f7d53242dc77c69fff7ced0755
#
# git describe --contains ad25ee36f00172f7d53242dc77c69fff7ced0755
# v6.9-rc1~159^2~210^2~273^2~70
# git tag --contains ad25ee36f00172f7d53242dc77c69fff7ced0755
# ...
CVE_STATUS[CVE-2023-7042] = "fixed-version: Fixed from version v6.9-rc1"

# linux-yocto-custom CVE-2024-0193     0.0      6.7      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2024-0193
#
# Upstream patch (Netfilter tree):
# https://git.kernel.org/pub/scm/linux/kernel/git/netfilter/nf.git/commit/?id=7315dc1e122c85ffdfc8defffbb8f8b616c2eb1a
# This was fixed for Fedora with the 6.6.10 stable kernel updates.
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2024-0193
# break-fix: 212ed75dc5fb9d1423b3942c8f872a868cda3466 7315dc1e122c85ffdfc8defffbb8f8b616c2eb1a
# upstream_linux: released (6.7, 5.15.146)
#
# It is patched here: git show 7315dc1e122c85ffdfc8defffbb8f8b616c2eb1a
# git describe --contains 7315dc1e122c85ffdfc8defffbb8f8b616c2eb1a
# v6.7~11^2~32^2
# git tag --contains 7315dc1e122c85ffdfc8defffbb8f8b616c2eb1a
CVE_STATUS[CVE-2024-0193] = "fixed-version: Fixed from version v6.7"

# linux-yocto-custom CVE-2024-38381    0.0      7.1      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2024-38381
#
# !!! https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2024-38381
# !!! break-fix: d24b03535e5eb82e025219c2f632b485409c898f e4a87abf588536d1cdfb128595e6e680af5cf3ed
# !!! upstream_linux: released (6.10~rc1, 5.4.278, 5.15.161)
#
# It is patched here: git show 017ff397624930fd7ac7f1761f3c9d6a7100f68c
# git describe --contains 017ff397624930fd7ac7f1761f3c9d6a7100f68c
# v6.9.4~137
# git tag --contains 017ff397624930fd7ac7f1761f3c9d6a7100f68c
CVE_STATUS[CVE-2024-38381] = "fixed-version: Fixed from version v6.9.4"

# linux-yocto-custom CVE-2024-50067    0.0      7.8      Unpatched  https://nvd.nist.gov/vuln/detail/CVE-2024-50067
#
# https://git.launchpad.net/ubuntu-cve-tracker/tree/active/CVE-2024-50067
# break-fix: dcad1a204f72624796ae83359403898d10393b9c 373b9338c9722a368925d83bc622c596896b328e
# upstream_linux: released (6.12~rc5, 6.11.6)
#
# It is patched here: git show 373b9338c9722a368925d83bc622c596896b328e
# git describe --contains 373b9338c9722a368925d83bc622c596896b328e
# v6.12-rc5~25^2~3
CVE_STATUS[CVE-2024-50067] = "fixed-version: Fixed from version v6.12-rc5"

# linux-yocto-custom_x.x.x.bb 
# attempt to have only kernel version related stuff in here

require recipes-kernel/linux/linux-yocto-custom.inc

KBRANCH = "linux-6.1.y"

LINUX_VERSION = "6.1.42"

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

# author	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2023-07-27 08:50:53 +0200
# committer	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2023-07-27 08:50:53 +0200
# commit	d2a6dc4eaf6d50ba32a9b39b4c6ec713a92072ab (patch)
# tree	        9040b58705e49619d0b2c10efe21e9bd1e8b0c86
# parent	1d4607f2a50c749e47a4b80030733cbf77c5570a (diff)
# download	linux-6.1.y.tar.gz

SRCREV ?= "d2a6dc4eaf6d50ba32a9b39b4c6ec713a92072ab"
#SRCREV_the-kernel:pn-linux-yocto-custom := "d2a6dc4eaf6d50ba32a9b39b4c6ec713a92072ab"
# in order to make buildhistory-collect-srcrevs happy:
SRCREV_the-kernel:pn-linux-yocto-custom := "${SRCREV}"

PATCHPATH="${THISDIR}/patch/6.1.x"

FILESEXTRAPATHS:prepend := "${PATCHPATH}:"

SRC_URI += "\
           file://multi-v7-ml-user-patches.scc \
           "
SRC_URI:append = " \
                file://${PATCHPATH}/patches;type=kmeta;destsuffix=patches \
                "

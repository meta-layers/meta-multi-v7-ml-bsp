# linux-yocto-custom_x.x.x.bb 
# attempt to have only kernel version related stuff in here

require recipes-kernel/linux/linux-yocto-custom.inc
#require recipes-kernel/linux/linux-yocto-custom-common_5.4.inc

KBRANCH = "linux-5.4.y"

LINUX_VERSION = "5.4.43"

LIC_FILES_CHKSUM = "file://COPYING;md5=bbea815ee2795b2f4230826c0c6b8814"

# author	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2020-05-27 17:46:53 +0200
# committer	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2020-05-27 17:46:53 +0200
# commit	e0d81ce760044efd3f26004aa32821c34968512a (patch)
# tree		02ddeca80abc563726d82053b54b7f19bdff5b59
# parent	b5100186021aff0d388aab48dfbfdb3c06955daf (diff)
# download	linux-e0d81ce760044efd3f26004aa32821c34968512a.tar.gz
# Linux 5.4.43 v5.4.43 linux-5.4.y

SRCREV ?= "e0d81ce760044efd3f26004aa32821c34968512a"

PATCHPATH="${THISDIR}/patch/5.4.x"

FILESEXTRAPATHS:prepend := "${PATCHPATH}:"

SRC_URI += "\
           file://multi-v7-ml-user-patches.scc \
           "
SRC_URI:append = " \
                file://${PATCHPATH}/patches;type=kmeta;destsuffix=patches \
                "

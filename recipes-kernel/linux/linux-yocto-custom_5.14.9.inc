# linux-yocto-custom_x.x.x.bb 
# attempt to have only kernel version related stuff in here

require recipes-kernel/linux/linux-yocto-custom.inc

KBRANCH = "linux-5.14.y"

LINUX_VERSION = "5.14.9"

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"


# author	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2021-09-30 10:13:08 +0200
# committer	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2021-09-30 10:13:08 +0200
# commit	70248e7b378b96f208d5544ee25b808a8ef2ddc2 (patch)
# tree		a139272bb4ab1375a9125dcdc3f8f076608a9c2c
# parent	f6fceb4e9ce6af58ddddfb0bad6b52da4f28dfaf (diff)
# download	linux-5.14.y.tar.gz

SRCREV ?= "70248e7b378b96f208d5544ee25b808a8ef2ddc2"

PATCHPATH="${THISDIR}/patch/5.14.x"

FILESEXTRAPATHS:prepend := "${PATCHPATH}:"

SRC_URI += "\
           file://multi-v7-ml-user-patches.scc \
           "
SRC_URI:append = " \
                file://${PATCHPATH}/patches;type=kmeta;destsuffix=patches \
                "

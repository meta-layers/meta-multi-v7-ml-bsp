# linux-yocto-custom_x.x.x.bb 
# attempt to have only kernel version related stuff in here

require recipes-kernel/linux/linux-yocto-custom.inc

KBRANCH = "linux-6.6.y"

LINUX_VERSION = "6.6.22"

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"


# author	Sasha Levin <sashal@kernel.org>	2024-03-13 07:48:36 -0400
# committer	Sasha Levin <sashal@kernel.org>	2024-03-15 14:25:07 -0400
# commit	6a646d9fe8a2bf8d25817ceddf96dfc5eb5446db (patch)
# tree		ac749a349b76dcc6c35fdd5108384e6376ee4da9
# parent	4a5b5bfea063745471af6395d22ebaea8242225e (diff)
# download	linux-6.6.y.tar.gz

SRCREV ?= "6a646d9fe8a2bf8d25817ceddf96dfc5eb5446db"
#SRCREV_the-kernel:pn-linux-yocto-custom := "d2a6dc4eaf6d50ba32a9b39b4c6ec713a92072ab"
# in order to make buildhistory-collect-srcrevs happy:
SRCREV_the-kernel:pn-linux-yocto-custom := "${SRCREV}"

PATCHPATH="${THISDIR}/patch/6.6.x"

FILESEXTRAPATHS:prepend := "${PATCHPATH}:"

SRC_URI += "\
           file://multi-v7-ml-user-patches.scc \
           "
SRC_URI:append = " \
                file://${PATCHPATH}/patches;type=kmeta;destsuffix=patches \
                "

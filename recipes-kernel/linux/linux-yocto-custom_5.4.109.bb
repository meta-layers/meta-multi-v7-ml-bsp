# linux-yocto-custom_x.x.x.bb 
# attempt to have only kernel version related stuff in here

require recipes-kernel/linux/linux-yocto-custom.inc
# we don't want this included everywhere, so move it furter up
#require recipes-kernel/linux/linux-yocto-custom-common_5.4.inc

KBRANCH = "linux-5.4.y"

LINUX_VERSION = "5.4.109"

LIC_FILES_CHKSUM = "file://COPYING;md5=bbea815ee2795b2f4230826c0c6b8814"

# author        Greg Kroah-Hartman <gregkh@linuxfoundation.org> 2021-03-30 14:35:30 +0200
# committer     Greg Kroah-Hartman <gregkh@linuxfoundation.org> 2021-03-30 14:35:30 +0200
# commit        4e85f8a712cddf2ceeaac50a26b239fbbcb7091f (patch)
# tree          200ff4a0473dcd3afb2938765928e83b6f087e0c
# parent        057dd3e6986b260f0bec68bd1f2cd23a5d9dbda3 (diff)
# download      linux-4e85f8a712cddf2ceeaac50a26b239fbbcb7091f.tar.gz
# Linux 5.4.109 v5.4.109

SRCREV ?= "4e85f8a712cddf2ceeaac50a26b239fbbcb7091f"

PATCHPATH="${THISDIR}/patch/5.4.x"

FILESEXTRAPATHS:prepend := "${PATCHPATH}:"

SRC_URI += "\
           file://multi-v7-ml-user-patches.scc \
           "
SRC_URI:append = " \
                file://${PATCHPATH}/patches;type=kmeta;destsuffix=patches \
                "

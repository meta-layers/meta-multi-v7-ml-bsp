# This is what should be common to all kernel versions
# This file was derived from the linux-yocto-custom.bb recipe in
# oe-core.
#
# linux-yocto-custom.bb:
#
#   A yocto-bsp-generated kernel recipe that uses the linux-yocto and
#   oe-core kernel classes to apply a subset of yocto kernel
#   management to git managed kernel repositories.
#
# Warning:
#
#   Building this kernel without providing a defconfig or BSP
#   configuration will result in build or boot errors. This is not a
#   bug.
#
# Notes:
#
#   patches: patches can be merged into to the source git tree itself,
#            added via the SRC_URI, or controlled via a BSP
#            configuration.
#
#   example configuration addition:
#            SRC_URI += "file://smp.cfg"
#   example patch addition:
#            SRC_URI += "file://0001-linux-version-tweak.patch
#   example feature addition:
#            SRC_URI += "file://feature.scc"
#

DEPENDS += "lzop-native "

# This version extension should match CONFIG_LOCALVERSION in defconfig
#LINUX_VERSION_EXTENSION ?= "-custom-ml-${KTYPE}"
LINUX_VERSION_EXTENSION ?= "-${KTYPE}"
PV = "${LINUX_VERSION}${LINUX_VERSION_EXTENSION}+git${SRCPV}"

# Sources, by default allow for the use of SRCREV pointing to orphaned tags/commits
#SRCBRANCH ?= ""
#SRCBRANCHARG = "${@['nobranch=1', 'branch=${SRCBRANCH}'][d.getVar('SRCBRANCH', True) != '']}"

#SRC_URI += " \
#             git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git;protocol=git;branch=${KBRANCH};${SRCBRANCHARG} \
#             file://multi-v7-ml-common;type=kmeta;destsuffix=multi-v7-ml-common \
#           "

### --> default
SRC_URI += "\
           git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git;protocol=git;branch=${KBRANCH};name=the-kernel \
           file://multi-v7-ml-common;type=kmeta;destsuffix=multi-v7-ml-common \
           "
# <-- default

#SRC_URI += "\
#           git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git;protocol=git;branch=${KBRANCH};name=machine \
#           file://multi-v7-ml-common;type=kmeta;destsuffix=multi-v7-ml-common;name=meta \
#           "

# in case we wanted to get it directly via gitpod:
#SRC_URI += "\
#           git://gp@gitpod.res.training/linux-stable.git;protocol=git;branch=${KBRANCH};user=gp:gp \
#           file://multi-v7-ml-common;type=kmeta;destsuffix=multi-v7-ml-common \
#           "


# for the phytec kernel:

#SRC_URI += "\
#           git://github.com/phytec/linux-phytec-mainline.git;protocol=https;branch=${KBRANCH};name=the-kernel \
#           file://multi-v7-ml-common;type=kmeta;destsuffix=multi-v7-ml-common \
#           "


SRCREV_the-kernel = "${SRCREV}"
SRCREV_FORMAT = "the-kernel_the-kernel"

#SRCREV_machine ?= "${SRCREV}"
#SRCREV_meta ?= "${SRCREV}"

#SRCREV_FORMAT ?= "machine_meta"

require recipes-kernel/linux/linux-yocto.inc

DESCRIPTION = "Mainline kernel"

require linux-ml-configs.inc
require linux-ml-machines.inc
# custom kernel config fragments:
inherit custom-kernel-features

# Let's try an in-tree defconfig:
KERNEL_DEFCONFIG:multi-v7-ml ?= "multi_v7_defconfig"
KBUILD_DEFCONFIG:multi-v7-ml ?= "multi_v7_defconfig"

KERNEL_DEFCONFIG:multi-v7 ?= "multi_v7_defconfig"
KBUILD_DEFCONFIG:multi-v7 ?= "multi_v7_defconfig"

KERNEL_DEFCONFIG:am335x-phytec-wega ?= "multi_v7_defconfig"
KBUILD_DEFCONFIG:am335x-phytec-wega ?= "multi_v7_defconfig"

#KERNEL_DEFCONFIG:imx6q-phytec-mira-rdk-nand ?= "imx_v6_v7_defconfig"
#KBUILD_DEFCONFIG:imx6q-phytec-mira-rdk-nand ?= "imx_v6_v7_defconfig"

KERNEL_DEFCONFIG:imx6q-phytec-mira-rdk-nand ?= "multi_v7_defconfig"
KBUILD_DEFCONFIG:imx6q-phytec-mira-rdk-nand ?= "multi_v7_defconfig"

KERNEL_DEFCONFIG:beagle-bone-black ?= "multi_v7_defconfig"
KBUILD_DEFCONFIG:beagle-bone-black ?= "multi_v7_defconfig"

KERNEL_DEFCONFIG:beagle-bone-black-conserver ?= "multi_v7_defconfig"
KBUILD_DEFCONFIG:beagle-bone-black-conserver ?= "multi_v7_defconfig"

KERNEL_DEFCONFIG:beagle-bone-green ?= "multi_v7_defconfig"
KBUILD_DEFCONFIG:beagle-bone-green ?= "multi_v7_defconfig"

KERNEL_DEFCONFIG:karo-imx6ul-txul ?= "multi_v7_defconfig"
KBUILD_DEFCONFIG:karo-imx6ul-txul ?= "multi_v7_defconfig"

KERNEL_DEFCONFIG:imx6ul-phytec-segin ?= "multi_v7_defconfig"
KBUILD_DEFCONFIG:imx6ul-phytec-segin ?= "multi_v7_defconfig"

KERNEL_DEFCONFIG:am335x-pocketbeagle ?= "multi_v7_defconfig"
KBUILD_DEFCONFIG:am335x-pocketbeagle ?= "multi_v7_defconfig"

KERNEL_DEFCONFIG:omap3-beagle-xm ?= "multi_v7_defconfig"
KBUILD_DEFCONFIG:omap3-beagle-xm ?= "multi_v7_defconfig"

KERNEL_DEFCONFIG:imx6sx-udoo-neo-full ?= "multi_v7_defconfig"
KBUILD_DEFCONFIG:imx6sx-udoo-neo-full ?= "multi_v7_defconfig"

KERNEL_DEFCONFIG:stm32mp157c-dk2 ?= "multi_v7_defconfig"
KBUILD_DEFCONFIG:stm32mp157c-dk2 ?= "multi_v7_defconfig"

KERNEL_DEFCONFIG:de0-nano-soc-kit ?= "multi_v7_defconfig"
KBUILD_DEFCONFIG:de0-nano-soc-kit ?= "multi_v7_defconfig"

KERNEL_DEFCONFIG:am335x-regor-rdk ?= "multi_v7_defconfig"
KBUILD_DEFCONFIG:am335x-regor-rdk ?= "multi_v7_defconfig"

KERNEL_DEFCONFIG:zynq-zed ?= "multi_v7_defconfig"
KBUILD_DEFCONFIG:zynq-zed ?= "multi_v7_defconfig"

KCONFIG_MODE="--alldefconfig"

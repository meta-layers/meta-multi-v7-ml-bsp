# linux-yocto-custom_x.x.x.bb 
# attempt to have only kernel version related stuff in here

require recipes-kernel/linux/linux-yocto-custom.inc

KBRANCH = "linux-5.15.y"
#KBRANCH = "v5.15.63-phy"

LINUX_VERSION = "5.15.63"

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"


# --> phytec kernel

# author	Andrej Picej <andrej.picej@norik.com>	2022-09-01 11:02:30 +0200
# committer	Christian Hemp <c.hemp@phytec.de>	2022-09-05 14:24:41 +0200
# commit	d06a5c9bc1a70f2cd257810612dc6af8c325c8a9 (patch)
# tree		ff3766ce61170f3ebeff0d88c7034fde315d4977
# parent	1c81ba02b0860248a2df4d6480d804d057a1c3ef (diff)
# download	linux-mainline-d06a5c9bc1a70f2cd257810612dc6af8c325c8a9.tar.bz2
# 		linux-mainline-d06a5c9bc1a70f2cd257810612dc6af8c325c8a9.zip

# Linux 5.15.63

# this is 5.15.63-phy
# SRCREV ?= "d06a5c9bc1a70f2cd257810612dc6af8c325c8a9"

# <-- phytec kernel


# --> upstream kernel

# author	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2022-08-25 11:40:49 +0200
# committer	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2022-08-25 11:40:49 +0200
# commit	addc9003c2e895fe8a068a66de1de6fdb4c6ac60 (patch)
# tree		788fef37688ad0075b35128b5ad4b8ec62e93564
# parent	b92be74cb2da0adbb349c8f1740de115adc5b4e6 (diff)
# download	linux-addc9003c2e895fe8a068a66de1de6fdb4c6ac60.tar.gz
# Linux 5.15.63 v5.15.63

# this is 5.15.63 upstream
SRCREV ?= "addc9003c2e895fe8a068a66de1de6fdb4c6ac60"

# <-- upstream kernel

PATCHPATH="${THISDIR}/patch/5.15.x"

FILESEXTRAPATHS:prepend := "${PATCHPATH}:"

SRC_URI += "\
           file://multi-v7-ml-user-patches.scc \
           "
SRC_URI:append = " \
                file://${PATCHPATH}/patches;type=kmeta;destsuffix=patches \
                "

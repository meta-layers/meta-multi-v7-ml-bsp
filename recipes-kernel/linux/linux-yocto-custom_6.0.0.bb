# linux-yocto-custom_x.x.x.bb 
# attempt to have only kernel version related stuff in here

require recipes-kernel/linux/linux-yocto-custom.inc

KBRANCH = "linux-6.0.y"

LINUX_VERSION = "6.0.0"

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"


# author	Linus Torvalds <torvalds@linux-foundation.org>	2022-10-02 14:09:07 -0700
# committer	Linus Torvalds <torvalds@linux-foundation.org>	2022-10-02 14:09:07 -0700
# commit	4fe89d07dcc2804c8b562f6c7896a45643d34b2f (patch)
# tree		b2f5b45b785a88b02ff4051850bcd9c4a2f309e7
# parent	a962b54e162c2977ff37905726cab29728380835 (diff)
# download	linux-4fe89d07dcc2804c8b562f6c7896a45643d34b2f.tar.gz
# Linux 6.0 v6.0 linux-6.0.y

SRCREV ?= "4fe89d07dcc2804c8b562f6c7896a45643d34b2f"

PATCHPATH="${THISDIR}/patch/6.0.x"

FILESEXTRAPATHS:prepend := "${PATCHPATH}:"

SRC_URI += "\
           file://multi-v7-ml-user-patches.scc \
           "
SRC_URI:append = " \
                file://${PATCHPATH}/patches;type=kmeta;destsuffix=patches \
                "

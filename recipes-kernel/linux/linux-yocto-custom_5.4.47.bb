# linux-yocto-custom_x.x.x.bb 
# attempt to have only kernel version related stuff in here

require recipes-kernel/linux/linux-yocto-custom.inc
# we don't want this included everywhere, so move it furter up
#require recipes-kernel/linux/linux-yocto-custom-common_5.4.inc

KBRANCH = "linux-5.4.y"

LINUX_VERSION = "5.4.47"

LIC_FILES_CHKSUM = "file://COPYING;md5=bbea815ee2795b2f4230826c0c6b8814"

# author	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2020-06-17 16:40:38 +0200
# committer	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2020-06-17 16:40:38 +0200
# commit	fd8cd8ac940c8b45b75474415291a3b941c865ab (patch)
# tree		cd03fd9b8ce421a7d433cdd630f75b17c5f5e862
# parent	d96ef8fa953428fe5cf050bd3ed31ac11fb8bfe9 (diff)
# download	linux-fd8cd8ac940c8b45b75474415291a3b941c865ab.tar.gz
# Linux 5.4.47 v5.4.47

SRCREV ?= "fd8cd8ac940c8b45b75474415291a3b941c865ab"

PATCHPATH="${THISDIR}/patch/5.4.x"

FILESEXTRAPATHS:prepend := "${PATCHPATH}:"

SRC_URI += "\
           file://multi-v7-ml-user-patches.scc \
           "
SRC_URI:append = " \
                file://${PATCHPATH}/patches;type=kmeta;destsuffix=patches \
                "

# linux-yocto-custom_x.x.x.bb 
# attempt to have only kernel version related stuff in here

require recipes-kernel/linux/linux-yocto-custom.inc

KBRANCH = "linux-5.15.y"

LINUX_VERSION = "5.15.69"

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"


# author	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2022-09-20 12:39:46 +0200
# committer	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2022-09-20 12:39:46 +0200
# commit	820b689b4a7a6ca1b4fdabf26a17196a2e379a97 (patch)
# tree		fa097f2501bf124d37d4ba50ccbdc9962af876c6
# parent	277674996dcf5e602b59aace532e63f2da8d5ea3 (diff)
# download	linux-820b689b4a7a6ca1b4fdabf26a17196a2e379a97.tar.gz
# Linux 5.15.69	v5.15.69	linux-5.15.y

# author	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2022-08-03 12:03:56 +0200
# committer	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2022-08-03 12:03:56 +0200
# commit	d676d6149a2f4b4d66b8ea0a1dfef30a54cf5750 (patch)
# tree		e53285c30765ada2461ef1c7112466a0d3716cf8
# parent	f0e42e43795db4c1054295c0d35e79203db83449 (diff)
# download	linux-d676d6149a2f4b4d66b8ea0a1dfef30a54cf5750.tar.gz

# Linux 5.15.59 v5.15.59 linux-5.15.y

SRCREV ?= "820b689b4a7a6ca1b4fdabf26a17196a2e379a97"

PATCHPATH="${THISDIR}/patch/5.15.x"

FILESEXTRAPATHS:prepend := "${PATCHPATH}:"

SRC_URI += "\
           file://multi-v7-ml-user-patches.scc \
           "
SRC_URI:append = " \
                file://${PATCHPATH}/patches;type=kmeta;destsuffix=patches \
                "

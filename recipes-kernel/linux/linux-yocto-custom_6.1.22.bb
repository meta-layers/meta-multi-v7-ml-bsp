# linux-yocto-custom_x.x.x.bb 
# attempt to have only kernel version related stuff in here

require recipes-kernel/linux/linux-yocto-custom.inc

KBRANCH = "linux-6.1.y"

LINUX_VERSION = "6.1.22"

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

# author	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2023-03-30 12:49:31 +0200
# committer	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2023-03-30 12:49:31 +0200
# commit	3b29299e5f604550faf3eff811d6cd60b4c6cae6 (patch)
# tree		79a6c04b5b366c6023314c0557dcbc19c94b33a6
# parent	8c31b663edc1cc5eb5c82282ab6ca99c69d1d942 (diff)
# download	linux-6.1.y.tar.gz
# Linux 6.1.22 v6.1.22 linux-6.1.y

SRCREV ?= "3b29299e5f604550faf3eff811d6cd60b4c6cae6"

PATCHPATH="${THISDIR}/patch/6.1.x"

FILESEXTRAPATHS:prepend := "${PATCHPATH}:"

SRC_URI += "\
           file://multi-v7-ml-user-patches.scc \
           "
SRC_URI:append = " \
                file://${PATCHPATH}/patches;type=kmeta;destsuffix=patches \
                "

# linux-yocto-custom_x.y.z.bb 
# attempt to have only kernel version related stuff in here

KTYPE="prt"

#PRT_PATCH = "fake-patch-5.10.27-rt35.patch.xz"
PRT_PATCH = "fake-patch-5.10.27-rt35.patch"

require recipes-kernel/linux/linux-yocto-custom_${PV}.inc

# we don't want this here:
#require recipes-kernel/linux/linux-yocto-custom-common_5.4.inc

# special stuff for prt:
require recipes-kernel/linux/linux-yocto-custom-common-prt_5.10.inc

# require prt common stuff
require recipes-kernel/linux/linux-yocto-custom-prt.inc


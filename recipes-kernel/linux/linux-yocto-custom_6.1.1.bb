# linux-yocto-custom_x.x.x.bb 
# attempt to have only kernel version related stuff in here

require recipes-kernel/linux/linux-yocto-custom.inc

KBRANCH = "linux-6.1.y"

LINUX_VERSION = "6.1.1"

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

# author	Linus Torvalds <torvalds@linux-foundation.org>	2022-12-11 14:15:18 -0800
# committer	Linus Torvalds <torvalds@linux-foundation.org>	2022-12-11 14:15:18 -0800
# commit	830b3c68c1fb1e9176028d02ef86f3cf76aa2476 (patch)
# tree		c7c1b7db9ced3eba518cfc1f711e9d89f73f8667
# parent	d92b86f672a42d9d74a24a63a1e59793c4116830 (diff)
# download	linux-830b3c68c1fb1e9176028d02ef86f3cf76aa2476.tar.gz
# Linux 6.1 v6.1 linux-6.1.y

# author	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2022-12-21 17:48:12 +0100
# committer	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2022-12-21 17:48:12 +0100
# commit	ebdb69c5b054f115ef5ff72f0bb2aaa1718904e6 (patch)
# tree		104c504eef86b616431f9cbf945d7418141e6264
# parent	1d1a710c1983819bdceaaae83cda309a84f51ea7 (diff)
# download	linux-6.1.y.tar.gz
# Linux 6.1.1 v6.1.1 linux-6.1.y

SRCREV ?= "ebdb69c5b054f115ef5ff72f0bb2aaa1718904e6"

PATCHPATH="${THISDIR}/patch/6.1.x"

FILESEXTRAPATHS:prepend := "${PATCHPATH}:"

SRC_URI += "\
           file://multi-v7-ml-user-patches.scc \
           "
SRC_URI:append = " \
                file://${PATCHPATH}/patches;type=kmeta;destsuffix=patches \
                "

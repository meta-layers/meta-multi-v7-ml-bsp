# linux-yocto-custom_x.x.x.bb 
# attempt to have only kernel version related stuff in here

require recipes-kernel/linux/linux-yocto-custom.inc

KBRANCH = "linux-6.1.y"

LINUX_VERSION = "6.1.37"

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

# author	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2023-07-01 13:16:27 +0200
# committer	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2023-07-01 13:16:27 +0200
# commit	0f4ac6b4c5f00f45b7a429c8a5b028a598c6400c (patch)
# tree		4a1e053ad0d4413c82845948514f41d7c74e1140
# parent	323846590c55fd9b05dfb9d768d76583a556d254 (diff)
# download	linux-0f4ac6b4c5f00f45b7a429c8a5b028a598c6400c.tar.gz
# Linux 6.1.37 v6.1.37 linux-6.1.y

SRCREV ?= "0f4ac6b4c5f00f45b7a429c8a5b028a598c6400c"

PATCHPATH="${THISDIR}/patch/6.1.x"

FILESEXTRAPATHS:prepend := "${PATCHPATH}:"

SRC_URI += "\
           file://multi-v7-ml-user-patches.scc \
           "
SRC_URI:append = " \
                file://${PATCHPATH}/patches;type=kmeta;destsuffix=patches \
                "

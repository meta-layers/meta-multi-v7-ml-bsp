# linux-yocto-custom_x.x.x.bb 
# attempt to have only kernel version related stuff in here

require recipes-kernel/linux/linux-yocto-custom.inc

KBRANCH = "linux-6.6.y"

LINUX_VERSION = "6.6.58"

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

# author	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2024-10-22 15:46:36 +0200
# committer	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2024-10-22 15:46:36 +0200
# commit	18916a684a8b836957df88438f9bca590799d04c (patch)
# tree	        af73604696b84a992c38eaee505834ec5c2d11c1
# parent	fd6e2af79a947e909c72b09f1ce3175f381067ef (diff)
# download	linux-6.6.y.tar.gz

SRCREV ?= "18916a684a8b836957df88438f9bca590799d04c"
#SRCREV_the-kernel:pn-linux-yocto-custom := "d2a6dc4eaf6d50ba32a9b39b4c6ec713a92072ab"
# in order to make buildhistory-collect-srcrevs happy:
SRCREV_the-kernel:pn-linux-yocto-custom := "${SRCREV}"

PATCHPATH="${THISDIR}/patch/6.6.x"

FILESEXTRAPATHS:prepend := "${PATCHPATH}:"

SRC_URI += "\
           file://multi-v7-ml-user-patches.scc \
           "
SRC_URI:append = " \
                file://${PATCHPATH}/patches;type=kmeta;destsuffix=patches \
                "

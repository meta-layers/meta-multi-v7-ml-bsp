# linux-yocto-custom_x.x.x.bb 
# attempt to have only kernel version related stuff in here

require recipes-kernel/linux/linux-yocto-custom.inc

KBRANCH = "linux-5.15.y"

LINUX_VERSION = "5.15.13"

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"


# author	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2022-01-05 12:42:40 +0100
# committer	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2022-01-05 12:42:40 +0100
# commit	734eb1fd2073f503f5c6b44f1c0d453ca6986b84 (patch)
# tree		ebe7353403344d108ddcda387cfb0d43543df4ce
# parent	bc5fce3dff9a7f3c75311128dcb17daf96776b82 (diff)
# download	linux-5.15.y.tar.gz

# Linux 5.15.13 v5.15.13 linux-5.15.y

SRCREV ?= "734eb1fd2073f503f5c6b44f1c0d453ca6986b84"

PATCHPATH="${THISDIR}/patch/5.15.x"

FILESEXTRAPATHS:prepend := "${PATCHPATH}:"

SRC_URI += "\
           file://multi-v7-ml-user-patches.scc \
           "
SRC_URI:append = " \
                file://${PATCHPATH}/patches;type=kmeta;destsuffix=patches \
                "

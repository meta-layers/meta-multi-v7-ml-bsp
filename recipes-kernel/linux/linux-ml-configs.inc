# This include file sets up the ml config fragments, these
# fragments build on top of the base config infrastructure provided in
# the linux-yocto/kernel-yocto recipes and classes in core layer of OE.

FILESEXTRAPATHS:prepend := "${THISDIR}/config:"

SRC_URI:append = " \
                file://multi-v7-ml-base;type=kmeta;destsuffix=multi-v7-ml-base \
                "

# from dunfell we need to do this crazy shit due to meta-virtualization changes
# those files are also available in meta-virtualization - might need to update them
# here we can override them?
#FILESEXTRAPATHS:prepend := "${THISDIR}/config/multi-v7-ml-base/features/xt-checksum:"
#FILESEXTRAPATHS:prepend := "${THISDIR}/config/multi-v7-ml-base/features/ebtables:"
#FILESEXTRAPATHS:prepend := "${THISDIR}/config/multi-v7-ml-base/features/vswitch:"
#FILESEXTRAPATHS:prepend := "${THISDIR}/config/multi-v7-ml-base/features/lxc:"
#FILESEXTRAPATHS:prepend := "${THISDIR}/config/multi-v7-ml-base/features/docker:"

# this is not available in meta-virtualization:
FILESEXTRAPATHS:prepend := "${THISDIR}/config/multi-v7-ml-base:"

# linux-yocto-custom_x.x.x.bb 
# attempt to have only kernel version related stuff in here

require recipes-kernel/linux/linux-yocto-custom.inc

KBRANCH = "linux-5.15.y"

LINUX_VERSION = "5.15.81"

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

# author	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2022-12-02 17:41:12 +0100
# committer	Greg Kroah-Hartman <gregkh@linuxfoundation.org>	2022-12-02 17:41:12 +0100
# commit	e4a7232c917cd1b56d5b4fa9d7a23e3eabfecba0 (patch)
# tree		5aa62728c1bb6a989dde29466ffe479323994645
# parent	5c5c563a0817f565611ead1e4f60d1198a89647f (diff)
# download	linux-e4a7232c917cd1b56d5b4fa9d7a23e3eabfecba0.tar.gz
# Linux 5.15.81 v5.15.81 linux-5.15.y

SRCREV ?= "e4a7232c917cd1b56d5b4fa9d7a23e3eabfecba0"

PATCHPATH="${THISDIR}/patch/5.15.x"

FILESEXTRAPATHS:prepend := "${PATCHPATH}:"

SRC_URI += "\
           file://multi-v7-ml-user-patches.scc \
           "
SRC_URI:append = " \
                file://${PATCHPATH}/patches;type=kmeta;destsuffix=patches \
                "

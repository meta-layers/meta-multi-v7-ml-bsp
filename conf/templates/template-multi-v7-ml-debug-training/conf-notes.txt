
### Shell environment set up for builds. ###

You can now run 'bitbake <target>'

Common targets are:
    core-image-minimal
    core-image-sato-sdk
    core-image-sato-sdk -c populate_sdk
    core-image-sato-sdk -c populate_sdk_ext
    buildtools-extended-tarball

You can also run generated qemu images with a command like 'runqemu qemux86'

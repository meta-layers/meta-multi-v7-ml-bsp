# in my BSPs I define many serial ports (consoles)
# since I want the same image to run on muliple boards
# this leads to warnings like that:
#      Feb 10 09:38:10 multi-v7-ml authpriv.err agetty[5446]: /dev/ttyS0: not a tty
#      Feb 10 09:38:10 multi-v7-ml authpriv.err agetty[5445]: /dev/ttyS2: not a tty
# so we hack a systemd unit file and change e.g. 
#      Restart=always -> Restart=on-failure
#      and various StartLimit settings
do_install:append() {
# 	Restart=always -> Restart=on-failure
        sed -i 's/Restart=always/Restart=on-failure/' ${D}${base_libdir}/systemd/system/serial-getty@.service
# 	add after this line:
#     		Restart=on-failure
# 	this is what we want to add:
#		RestartSec=0
# 		StartLimitAction=none
#		StartLimitBurst=3
#		StartLimitInterval=60
        sed -i '/^Restart=on-failure/a RestartSec=0\nStartLimitAction=none\nStartLimitBurst=3\nStartLimitInterval=60\n' ${D}${base_libdir}/systemd/system/serial-getty@.service
}

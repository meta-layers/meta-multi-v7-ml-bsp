# e.g. in local.conf:
# --> docker
# CONTAINER_ON_PERSISTENT = "1"
# CONTAINER_ON_PERSISTENT_FSTYPE = "btrfs"
# DEVICE_FOR_CONTAINER = "/dev/mmcblk0p2"
# MOUNT_FOR_CONTAINER = "/var/lib/docker"
# <-- docker
# --> podman
# CONTAINER_ON_PERSISTENT = "1"
# CONTAINER_ON_PERSISTENT_FSTYPE = "btrfs"
# DEVICE_FOR_CONTAINER = "/dev/mmcblk0p2"
# MOUNT_FOR_CONTAINER = "/var/lib/containers"
# <-- podman

do_install:append () {
    # in local.conf:
    #    CONTAINER_ON_PERSISTENT = "1"

    if [[ ${CONTAINER_ON_PERSISTENT} = *1* ]]
    then
    cat >> ${D}${sysconfdir}/fstab <<EOF

# e.g. with rootfs over nfs (but not only)
# we mount ${MOUNT_FOR_CONTAINER} on ${DEVICE_FOR_CONTAINER} of type ${CONTAINER_ON_PERSISTENT_FSTYPE}
# which resides e.g. on the SD card
${DEVICE_FOR_CONTAINER}  ${MOUNT_FOR_CONTAINER} ${CONTAINER_ON_PERSISTENT_FSTYPE}   defaults        0       0
EOF
   fi # CONTAINER_ON_PERSISTENT

}


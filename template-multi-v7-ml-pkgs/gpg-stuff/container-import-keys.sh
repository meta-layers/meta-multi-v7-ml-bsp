#!/bin/bash
######## import the keys ########
set -x
gpg --fingerprint

gpg --list-keys

gpg --list-secret-keys

pushd /workdir/sources/keys-for-ipk-signing/backup

gpg --import ipkkeypub.gpg

gpg --list-keys

#gpg --allow-secret-key-import --pinentry-mode loopback --import ipkkeypriv.gpg
# automate the line above
passwd=$(cat /workdir/sources/keys-for-ipk-signing/passphrase/passphrase.txt)
gpg --batch --allow-secret-key-import --pinentry-mode loopback --import ipkkeypriv.gpg --passphrase ${passwd}

gpg --list-secret-keys

gpg --fingerprint

popd

######## trust the keys ########

#gpg --edit-key ipkkey
# automate the line above
(echo trust &echo 5 &echo y &echo quit) | gpg --command-fd 0 --edit-key ipkkey

gpg --fingerprint

gpg --list-keys

gpg --list-secret-keys
set +x

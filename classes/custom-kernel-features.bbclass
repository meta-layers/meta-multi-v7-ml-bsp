#
# Class to place yocto-kernel-cache fragments
# selections useful to anyone
#
python __anonymous() {
    if not (bb.data.inherits_class('kernel-yocto', d)):
        bb.warn("distro-custom-kernel-features class being inherited without kernel-yocto")
}

#KERNEL_FEATURES:append = " ${@bb.utils.contains("DISTRO_FEATURES", "apparmor", " features/apparmor/apparmor.scc", "" ,d)}"
#KERNEL_FEATURES:append = " ${@bb.utils.contains("DISTRO_FEATURES", "smack", " features/smack/smack.scc", "" ,d)}"
#KERNEL_FEATURES:append = " ${@bb.utils.contains("DISTRO_FEATURES", "dm-verity", " features/device-mapper/dm-verity.scc", "" ,d)}"
#KERNEL_FEATURES:append = " ${@bb.utils.contains_any("MACHINE_FEATURES", "tpm tpm2", " features/tpm/tpm.scc","", d)}"

KERNEL_FEATURES:append = " ${@bb.utils.contains("MACHINE_FEATURES", "usb-console", " features/usb-acm/usb-acm.scc features/usb-serial/usb-serial.scc", "" ,d)}"
# if DISTRO_FEATURES contains soft-watchdog we'll add user space watchdog and this needs this kernel feature:
KERNEL_FEATURES:append = " ${@bb.utils.contains("DISTRO_FEATURES", "soft-watchdog", " features/soft-watchdog/soft-watchdog.scc", "" ,d)}"
KERNEL_FEATURES:append = " ${@bb.utils.contains("MACHINE_FEATURES", "vfat", " features/vfat/vfat.scc", "", d)}"
# this comes in from poky/linux-yocto.inc, let's remove it since currently we don't have a yocto kernel cache:
KERNEL_FEATURES:remove = "cfg/fs/vfat.scc"

# --> meta-virt fixes
KERNEL_FEATURES:remove = "cfg/virtio.scc"
# <-- meta-virt fixes
